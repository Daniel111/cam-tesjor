package kh.com.camtesjorapp.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import kh.com.camtesjorapp.R;

public class UserTrackingInfo implements ClusterItem {

    private Bitmap mBitmapLogo;
    private LatLng latLng;
    private ClusterManager<UserTrackingInfo> mClusterManager;
    private String mEmail;
    private String mName;
    private String mImageUrl;
    private TrackingModel trackingModel;
    private int mIndexItSelf;

    public UserTrackingInfo(Context context, ClusterManager<UserTrackingInfo> clusterManager, TrackingModel trackingModel) {
        this.mClusterManager = clusterManager;
        this.trackingModel = trackingModel;
        this.latLng = new LatLng(trackingModel.getLat(), trackingModel.getLng());
        setmEmail(trackingModel.getEmail());
        setmImageUrl(trackingModel.getImage_url());
        setmName(trackingModel.getFull_name());
        setImageProfile(context);
    }

    @Override
    public LatLng getPosition() {
        return getLatLng();
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getSnippet() {
        return null;
    }

    private void setImageProfile(Context context){

        Glide.with(context).load(getmImageUrl()).
                asBitmap().placeholder(R.mipmap.ic_launcher)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .override(100, 100)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        setmBitmapLogo(resource);
                        if (null != mClusterManager) {
                            mClusterManager.addItem(UserTrackingInfo.this);
                            mClusterManager.cluster();
                        } else {
                            Log.w("CustomClusterManager", "CustomClusterManager is null");
                        }
                    }
                });
    }

    public Bitmap getmBitmapLogo() {
        return mBitmapLogo;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getmEmail() {
        return mEmail;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmImageUrl() {
        return mImageUrl;
    }

    public void setmImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }

    public void setmBitmapLogo(Bitmap mBitmapLogo) {
        this.mBitmapLogo = mBitmapLogo;
    }

    public TrackingModel getTrackingModel() {
        return trackingModel;
    }

    public int getmIndexItSelf() {
        return mIndexItSelf;
    }

    public void setmIndexItSelf(int mIndexItSelf) {
        this.mIndexItSelf = mIndexItSelf;
    }
}
