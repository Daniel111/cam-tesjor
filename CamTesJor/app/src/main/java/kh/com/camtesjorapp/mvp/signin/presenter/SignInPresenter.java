package kh.com.camtesjorapp.mvp.signin.presenter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import org.json.JSONObject;
import kh.com.camtesjorapp.mvp.signin.view.ISignInVIew;

public class SignInPresenter implements ISignInPresenter {

    private FirebaseAuth mAuth;
    private ISignInVIew mISignInVIew;
    private Context mContext;

    public SignInPresenter(Context context) {
        this.mISignInVIew = (ISignInVIew)context;
        this.mContext = context;
        this.mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void retrieveData() {

    }

    @Override
    public void onSignInFacebook(LoginButton loginButton, CallbackManager callbackManager) {

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                mISignInVIew.onFacebookSignInSucceed(loginResult);
            }

            @Override
            public void onCancel() {
                mISignInVIew.onFacebookSignInCanceled();
            }

            @Override
            public void onError(FacebookException error) {
                mISignInVIew.onFacebookSignInFailed(error);
            }
        });

    }

    @Override
    public void onEmailSignIn(String email, String pass) {

        mAuth.signInWithEmailAndPassword(email, pass).addOnCompleteListener( new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) mISignInVIew.onEmailSignInSucceed(task);
                else mISignInVIew.onEmailSignInFailed(task);
            }
        });

    }

    @Override
    public void onTokenSignIn(String token) {

        AuthCredential credential = FacebookAuthProvider.getCredential(token);
        mAuth.signInWithCredential(credential).addOnCompleteListener((Activity) mContext, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()) mISignInVIew.onTokenSignInSucceed(task);
                else mISignInVIew.onTokenSignInFailed(task);
            }

        });
    }

    @Override
    public void onFbDataRequest(final LoginResult loginResult) {

        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        mISignInVIew.onFbDataRespond(object,loginResult);
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender,birthday,picture.type(large)");
        request.setParameters(parameters);
        request.executeAsync();

    }

    @Override
    public void onFireBaseUserDataRequest() {

        final String uId = mAuth.getUid();
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("users");

        if (uId != null)
            databaseReference.child(uId).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mISignInVIew.onFirebaseUserDataRespond(dataSnapshot);
                    databaseReference.removeEventListener(this);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    databaseReference.removeEventListener(this);
                }
            });

    }

    @Override
    public void onGplusSignIn() {

    }
}
