package kh.com.camtesjorapp.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.sharepref.UserSharePref;
import kh.com.camtesjorapp.ui.controlmember.ControlMemberActivity;

public class SettingFragment extends Fragment{

    private Context mContext;
    private RadioButton mCheckboxYourCircle;
    private RadioButton mCheckBoxJoiningCircle;
    private UserSharePref mUserSharePref;
    private ControlMemberActivity mControlMemberActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mControlMemberActivity = (ControlMemberActivity) mContext;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    private void init(View view){

        mCheckBoxJoiningCircle = view.findViewById(R.id.checkbox_joining_circle);
        mCheckboxYourCircle = view.findViewById(R.id.checkbox_your_circle);
        mCheckBoxJoiningCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCheckChange(view);
            }
        });
        mCheckboxYourCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCheckChange((view));
            }
        });

        setFocusingCircle();
    }

    private void setFocusingCircle(){

        mUserSharePref = UserSharePref.getInstance(mContext);
        if(mUserSharePref == null) return;

        String currentCircle = mUserSharePref.getCurrentCircle();
        String yourCircle = mUserSharePref.getUserInfo(mContext).getGroup_admin();

        if(yourCircle.equals(currentCircle)){
            mCheckBoxJoiningCircle.setChecked(false);
            mCheckboxYourCircle.setChecked(true);
        }else{
            mCheckBoxJoiningCircle.setChecked(true);
            mCheckboxYourCircle.setChecked(false);
        }
    }

    private void onCheckChange(View v){

        String currentCircle = mUserSharePref.getCurrentCircle();
        String yourCircle = mUserSharePref.getUserInfo(mContext).getGroup_admin();

        if(R.id.checkbox_joining_circle == v.getId()){
            if(currentCircle.equals(yourCircle) ) {
                mCheckboxYourCircle.setChecked(false);
                mUserSharePref.setCurrentCircle(
                        mUserSharePref.getUserInfo(mContext).getGroup_joining());
                refreshActivity();
            }
        }else {
            if(!currentCircle.equals(yourCircle) ) {
                mCheckBoxJoiningCircle.setChecked(false);
                mUserSharePref.setCurrentCircle(
                        mUserSharePref.getUserInfo(mContext).getGroup_admin());
                refreshActivity();
            }
        }
    }

    private void refreshActivity(){
        mControlMemberActivity.finish();
        mControlMemberActivity.startActivity(mControlMemberActivity.getIntent());
    }
}
