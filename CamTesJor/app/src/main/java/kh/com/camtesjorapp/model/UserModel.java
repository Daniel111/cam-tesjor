package kh.com.camtesjorapp.model;


public class UserModel {

    private String first_name;
    private String last_name;
    private String full_name;
    private String email;
    private String uid;
    private String group_admin;
    private String group_joining;
    private String image_url;

    public UserModel(){}

    public UserModel(String first_name, String last_name, String full_name, String email, String uid, String group_admin, String group_joining, String image_url) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.full_name = full_name;
        this.email = email;
        this.uid = uid;
        this.group_admin = group_admin;
        this.group_joining = group_joining;
        this.image_url = image_url;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getGroup_admin() {
        return group_admin;
    }

    public void setGroup_admin(String group_admin) {
        this.group_admin = group_admin;
    }

    public String getGroup_joining() {
        return group_joining;
    }

    public void setGroup_joining(String group_joining) {
        this.group_joining = group_joining;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}
