package kh.com.camtesjorapp.ui.resort.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.ui.resortdetail.activity.ResortDetailActivity;
import kh.com.camtesjorapp.model.ResortModel;

/**
 * Created by mac on 5/25/2017 AD.
 */

public class ResortAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context mContext;
    private List<ResortModel> mResortModels;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 4;
    private int lastVisibleItem, totalItemCount;

    public ResortAdapter(Context context, List<ResortModel> resortModels){
        this.mContext = context;
        this.mResortModels = resortModels;
    }

    public void setModels(List<ResortModel> resortModels){
        this.mResortModels = resortModels;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        int VIEW_TYPE_ITEM = 1;
        if(viewType == VIEW_TYPE_ITEM){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.resorts_item, parent, false);
            return new ResortAdapter.ViewHolder(v);
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.loading, parent, false);
            return new ResortAdapter.ProgressViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof ResortAdapter.ViewHolder) {

            ResortAdapter.ViewHolder itemHolder = (ResortAdapter.ViewHolder) holder;
            itemHolder.resortName.setText(mResortModels.get(position).getName());
            itemHolder.resortDetail.setText(mResortModels.get(position).getDescription());
            Glide.with(mContext).
                    load(mResortModels.get(position).getImage_url()).
                    into(itemHolder.resortImage);

        } else if (holder instanceof ResortAdapter.ProgressViewHolder) {

            ProgressViewHolder loadingViewHolder = (ProgressViewHolder) holder;
            loadingViewHolder.mProgressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return mResortModels.size() + (isLoading ? 1 : 0);
    }

    @Override
    public int getItemViewType(int position) {
        return (position < mResortModels.size()) ? 1 : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private ImageView resortImage;
        private TextView resortName;
        private TextView resortDetail;

        ViewHolder(View itemView) {
            super(itemView);

            resortImage = (ImageView)itemView.findViewById(R.id.img_tourist_area);
            resortName = (TextView)itemView.findViewById(R.id.tv_tourist_area_title);
            resortDetail = (TextView)itemView.findViewById(R.id.tv_tourist_area_detail);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            ResortDetailActivity.lunch(mContext,
                    mResortModels.get(getAdapterPosition()).getResortDetailName(),
                    mResortModels.get(getAdapterPosition()).getName()
                    );
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {

        ProgressBar mProgressBar;
        private StaggeredGridLayoutManager.LayoutParams mLayoutParams = new StaggeredGridLayoutManager.
                LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        ProgressViewHolder(View itemView) {
            super(itemView);
            mProgressBar = (ProgressBar ) itemView.findViewById(R.id.progressBar1);
            mLayoutParams.setFullSpan(true);
            itemView.setLayoutParams(mLayoutParams);
        }
    }

    public void onLoadMore(RecyclerView recyclerView) {

        final StaggeredGridLayoutManager layoutManager = (StaggeredGridLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = layoutManager.getItemCount();
                lastVisibleItem = layoutManager.findLastVisibleItemPositions(null)[0];

                if (mResortModels.size() > 0 && !isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                        isLoading = true;
                    }
                }
            }
        });
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public void setLoaded() {
        isLoading = false;
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

}
