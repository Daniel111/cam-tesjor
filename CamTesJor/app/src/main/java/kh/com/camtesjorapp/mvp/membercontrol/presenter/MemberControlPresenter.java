package kh.com.camtesjorapp.mvp.membercontrol.presenter;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import kh.com.camtesjorapp.model.BatteryModel;
import kh.com.camtesjorapp.model.TrackingModel;
import kh.com.camtesjorapp.mvp.membercontrol.view.IMemberControlView;

public class MemberControlPresenter implements IMemberControlPresenter{

    private IMemberControlView mIMemberControlView;

    public MemberControlPresenter(IMemberControlView mIMemberControlView) {
        this.mIMemberControlView = mIMemberControlView;
    }

    @Override
    public void onChildLocationEvent(String groupId) {

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("group").child(groupId);
        databaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                TrackingModel trackingModel = dataSnapshot.getValue(TrackingModel.class);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                TrackingModel trackingModel = dataSnapshot.getValue(TrackingModel.class);
                mIMemberControlView.onChildChange(trackingModel,s);

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                TrackingModel trackingModel = dataSnapshot.getValue(TrackingModel.class);
                mIMemberControlView.onChildRemove(trackingModel);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onChildBatteryEvent(String groupId) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("group_battery").child(groupId);
        databaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                BatteryModel batteryModel = dataSnapshot.getValue(BatteryModel.class);
                mIMemberControlView.onChildBatteryChange(batteryModel,dataSnapshot.getKey());

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                BatteryModel batteryModel = dataSnapshot.getValue(BatteryModel.class);
                mIMemberControlView.onChildBatteryRemove(batteryModel);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mIMemberControlView.onChildBatteryEventCancel(databaseError);
            }
        });


    }
}
