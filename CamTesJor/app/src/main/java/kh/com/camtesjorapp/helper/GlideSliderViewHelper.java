package kh.com.camtesjorapp.helper;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.DrawableTypeRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;

import static com.bumptech.glide.load.engine.DiskCacheStrategy.RESULT;


/**
 * Created by Bradley on 3/2/2017.
 */

public class GlideSliderViewHelper extends DefaultSliderView {

    private RequestManager mGlide;

    public GlideSliderViewHelper(Context context) {
        super(context);
    }

    protected void bindEventAndShow(final View v, ImageView targetImageView) {
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnSliderClickListener != null) {
                    mOnSliderClickListener.onSliderClick(GlideSliderViewHelper.this);
                }
            }
        });

        if (targetImageView == null)
            return;

        RequestManager p = (mGlide != null) ? mGlide : Glide.with(mContext);
        DrawableTypeRequest rq = null;
        if (getUrl() != null) {
        rq = p.load(getUrl());
            rq.diskCacheStrategy(RESULT).skipMemoryCache(true);
        } else {
            return;
        }
        if (getEmpty() != 0) {
            rq.placeholder(getEmpty());
        }
        if (getError() != 0) {
            rq.error(getError());
        }
        v.findViewById(com.daimajia.slider.library.R.id.loading_bar).setVisibility(View.INVISIBLE);
        rq.diskCacheStrategy(RESULT).skipMemoryCache(true).centerCrop().into(targetImageView);
    }
}

