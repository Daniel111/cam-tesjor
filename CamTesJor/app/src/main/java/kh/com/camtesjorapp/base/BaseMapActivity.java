package kh.com.camtesjorapp.base;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.ClusterManager;

import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.controller.MarkerMapRender;
import kh.com.camtesjorapp.interfaces.LocationUpdaterInterface;
import kh.com.camtesjorapp.model.UserTrackingInfo;
import kh.com.camtesjorapp.controller.LocationRequestController;

@SuppressLint("Registered")
public class BaseMapActivity extends BaseActivity implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener,
        GoogleMap.OnMyLocationButtonClickListener,
        LocationUpdaterInterface, GoogleMap.OnMarkerDragListener {

    private ClusterManager<UserTrackingInfo> mClusterManager;
    private GoogleMap gMap;
    private LatLng latLng = null;
    private LocationRequestController mLocationRequestController;
    private MarkerMapRender markerMapRender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        this.gMap = googleMap;
        googleMap.setOnMapClickListener(this);
        mClusterManager = new ClusterManager<UserTrackingInfo>(BaseMapActivity.this, gMap) {

            @Override
            public void onCameraIdle() {
                super.onCameraIdle();
            }
        };
        markerMapRender = new MarkerMapRender(this, gMap, mClusterManager);
        mClusterManager.setRenderer(markerMapRender);
        gMap.setOnCameraIdleListener(mClusterManager);
        gMap.setOnMarkerClickListener(mClusterManager);
        gMap.setOnMyLocationButtonClickListener(this);
        googleMap.setOnMapClickListener(this);
        mLocationRequestController.checkPermissions();
    }

    @Override
    public void onMapClick(LatLng latLng) {
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onLocationCallBack(Object type, Location location) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
//            gMap.setMyLocationEnabled(true);
        }
        if (location != null) {
            if (latLng == null) {
                latLng = new LatLng(location.getLatitude(), location.getLongitude());
            }
        } else {
            if (latLng == null) {
                latLng = new LatLng(0, 0);
            }
        }
        if (null != gMap && null != mClusterManager) {
            gMap.clear();
            mClusterManager.clearItems();
        }
        gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17), 1, null);
        gMap.setOnMarkerDragListener(this);
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {

    }

    public void init() {
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mLocationRequestController = new LocationRequestController(this, LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequestController.setLocationUpdateListener(this);
    }

    public ClusterManager<UserTrackingInfo> getmClusterManager() {
        return mClusterManager;
    }

    public GoogleMap getgMap() {
        return gMap;
    }

    public MarkerMapRender getMarkerMapRender() {
        return markerMapRender;
    }

    public LocationRequestController getmLocationRequestController() {
        return mLocationRequestController;
    }

    public void setmLocationRequestController(LocationRequestController mLocationRequestController) {
        this.mLocationRequestController = mLocationRequestController;
    }
}
