package kh.com.camtesjorapp.ui.resort.presenter;

import android.content.Context;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.Timer;
import java.util.TimerTask;
import javax.annotation.Nullable;
import kh.com.camtesjorapp.constant.Constants;
import kh.com.camtesjorapp.ui.resort.activity.ResortActivity;
import kh.com.camtesjorapp.ui.resort.view.IResortView;
import kh.com.camtesjorapp.utils.NetworkUtil;

public class ResortPresenterCompl implements IResortPresenter, ValueEventListener {

    private IResortView mIResortView;
    private String mProvinceName;
    private String mLastKey;
    private DatabaseReference resorts;
    private boolean isGotResult;
    private Context context;
    private String resortType;

    public ResortPresenterCompl(IResortView iResortView, String resortType, @Nullable String provinceName){
        this.mIResortView = iResortView;
        this.mProvinceName = provinceName;
        this.context = (Context) iResortView;
        this.resortType = resortType;
//        init();
    }

    public void init(){
        resorts = FirebaseDatabase.getInstance().getReference().
                child(Constants.RESORT_DETAIL_PATH);
        resorts.keepSynced(true);
        switch (resortType){
            case ResortActivity.RESORT_BY_PROVINCE:
                retrieveProvinceData();
                break;
            case ResortActivity.POPULAR_RESORT:
                retrievePopularData();
                break;
            case ResortActivity.ALL_RESORT:
                retrieveData();
                break;
        }
    }

    public ResortPresenterCompl(IResortView iResortView,String lastKey){
        this.mIResortView = iResortView;
        this.mLastKey = lastKey;
        resorts = FirebaseDatabase.getInstance().getReference().
                child(Constants.RESORT_DETAIL_PATH);
    }

    @Override
    public void retrieveData() {
        resorts.orderByKey().limitToFirst(20).addListenerForSingleValueEvent(this);
        onRequestTimeout();
    }

    @Override
    public void retrieveLoadMoreData() {
        resorts.orderByKey().startAt(mLastKey).limitToFirst(20).addListenerForSingleValueEvent(this);
    }

    @Override
    public void retrievePopularData() {
        resorts.orderByChild("popular").equalTo(true).addListenerForSingleValueEvent(this);
        onRequestTimeout();
    }

    @Override
    public void retrieveAllData() {
        resorts.addListenerForSingleValueEvent(this);
        onRequestTimeout();
    }

    @Override
    public void retrieveProvinceData() {
        resorts.orderByChild("province").equalTo(mProvinceName).addListenerForSingleValueEvent(this);
        onRequestTimeout();
    }


    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        mIResortView.onRetrieveDataSucceed(dataSnapshot);
        resorts.removeEventListener(this);
        isGotResult = true;
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        mIResortView.onRetrieveDataError(databaseError);
        resorts.removeEventListener(this);
        isGotResult = true;
    }

    private void onRequestTimeout(){

        boolean isHaveInternet = NetworkUtil.getConnectivityStatus(context) != NetworkUtil.TYPE_NOT_CONNECTED;

        if(!isHaveInternet) {
            final Timer timer = new Timer();
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    timer.cancel();
                    if (!isGotResult) {
//                        resorts.removeEventListener(ResortPresenterCompl.this);
                        mIResortView.onRetrieveDataError(null);
                    }
                }
            };
            timer.schedule(timerTask, 100);
        }
    }
}
