package kh.com.camtesjorapp.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;

import kh.com.camtesjorapp.R;

public class MemberViewHolder extends RecyclerView.ViewHolder {

    public RoundedImageView avatar;
    public TextView textViewName;

    public MemberViewHolder(View itemView) {
        super(itemView);
        avatar = itemView.findViewById(R.id.round_image_view_avatar);
        textViewName = itemView.findViewById(R.id.text_view_name);
    }
}
