package kh.com.camtesjorapp.ui.resort.view;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

import kh.com.camtesjorapp.ui.resort.presenter.ResortPresenterCompl;

/**
 * Created by mac on 5/25/2017 AD.
 */

public interface IResortView {
    void onRetrieveDataSucceed(DataSnapshot snapshot);
    void onRetrieveDataError(DatabaseError databaseError);
}
