package kh.com.camtesjorapp.share;
import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.firebase.client.Firebase;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.onesignal.OneSignal;

import kh.com.camtesjorapp.base.BaseActivity;
import kh.com.camtesjorapp.base.CamTesjorActivity;
import kh.com.camtesjorapp.service.MyNotificationOpenedHandler;
import kh.com.camtesjorapp.service.MyNotificationReceivedHandler;

public class CamTesjorApp extends Application {

    private static Context context;

    public static Context getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        context = getApplicationContext();

        Firebase.setAndroidContext(this);
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        // OneSignal Initialization
        OneSignal.startInit(this)
                .setNotificationReceivedHandler(new MyNotificationReceivedHandler())
                .setNotificationOpenedHandler(new MyNotificationOpenedHandler())
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.None)
                .init();
    }
}
