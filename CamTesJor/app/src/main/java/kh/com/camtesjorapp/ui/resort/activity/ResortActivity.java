package kh.com.camtesjorapp.ui.resort.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.onesignal.OSNotification;

import java.util.ArrayList;
import java.util.List;

import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.ui.resort.adapter.ResortAdapter;
import kh.com.camtesjorapp.base.BaseActivity;
import kh.com.camtesjorapp.constant.Constants;
import kh.com.camtesjorapp.model.ResortModel;
import kh.com.camtesjorapp.ui.resort.presenter.ResortPresenterCompl;
import kh.com.camtesjorapp.ui.resort.view.IResortView;

public class ResortActivity extends BaseActivity implements IResortView, ResortAdapter.OnLoadMoreListener, TextWatcher {

    private ProgressBar progressBar;
    private ResortAdapter resortAdapter;
    private List<ResortModel> resortModels = new ArrayList<>();
    public static final String ALL_RESORT = "all_resort";
    public static final String POPULAR_RESORT = "popular_resort";
    public static final String RESORT_BY_PROVINCE = "resort_by_province";
    private String mLastKey;
    private String mPreviouseKey = "";
    private String mSearchedKey = "";
    private ResortPresenterCompl resortPresenterCompl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resort);
        init();
    }

    @Override
    protected String getToolbarTitle() {
//        return getResources().getString(R.string.tourist_attraction);
        return "";
    }

    @Override
    public void onRetrieveDataSucceed(DataSnapshot snapshot) {
        progressBar.setVisibility(View.GONE);
        setData(snapshot);
    }

    @Override
    public void refresh() {
        if(resortModels.size() == 0){
            progressBar.setVisibility(View.VISIBLE);
            findViewById(R.id.txt_no_data).setVisibility(View.GONE);
            resortPresenterCompl.init();
        }
    }

//    @Override
//    public void onNotificationReceived(OSNotification notification) {
//        super.onNotificationReceived(notification);
//    }

    private void setData(DataSnapshot snapshot) {

        if (getRequestType().equals(ALL_RESORT) && mSearchedKey.isEmpty()) {
            for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                ResortModel resortModel = dataSnapshot.getValue(ResortModel.class);
                mLastKey = dataSnapshot.getKey();
                if (!mPreviouseKey.equals(dataSnapshot.getKey()))
                    resortModels.add(resortModel);
            }
            if (!mPreviouseKey.equals(mLastKey)) {
                mPreviouseKey = mLastKey;
                resortAdapter.notifyItemRangeChanged(resortAdapter.getItemCount(), resortModels.size());
                resortAdapter.setLoaded();
            }
        } else {
            resortModels.clear();
            resortAdapter.notifyDataSetChanged();
            for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                ResortModel resortModel = dataSnapshot.getValue(ResortModel.class);

                if (!mSearchedKey.isEmpty()) {
                    if (resortModel != null && resortModel.getName().toLowerCase().contains(mSearchedKey.toLowerCase()))
                        resortModels.add(resortModel);
                } else
                    resortModels.add(resortModel);
            }
            resortAdapter.notifyItemRangeChanged(resortAdapter.getItemCount(), resortModels.size());
        }
        findViewById(R.id.txt_no_data).setVisibility(resortModels.size() != 0 ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onRetrieveDataError(DatabaseError databaseError) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
                findViewById(R.id.txt_no_data).setVisibility(resortModels.size() != 0 ? View.GONE : View.VISIBLE);
            }
        });
    }

    @Override
    public void onLoadMore() {
        new ResortPresenterCompl(ResortActivity.this, mLastKey).retrieveLoadMoreData();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if (getRequestType().equals(ALL_RESORT)) {
            resortPresenterCompl.retrieveAllData();
            if (s.toString().isEmpty())
                resortAdapter.setOnLoadMoreListener(this);
            else {
                resortAdapter.setOnLoadMoreListener(null);
                resortAdapter.setLoaded();
            }
        } else if (getRequestType().equals(POPULAR_RESORT))
            resortPresenterCompl.retrievePopularData();
        else resortPresenterCompl.retrieveProvinceData();
        mSearchedKey = s.toString();
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    private void init() {

        progressBar = (ProgressBar) findViewById(R.id.resort_progress_bar);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rec_resort);
        resortAdapter = new ResortAdapter(this, resortModels);
        EditText searchBox = (EditText) findViewById(R.id.edt_search);
        searchBox.addTextChangedListener(this);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        recyclerView.setAdapter(resortAdapter);
        resortAdapter.onLoadMore(recyclerView);
        if (getRequestType().equals(ALL_RESORT))
            resortAdapter.setOnLoadMoreListener(this);
        resortPresenterCompl = new ResortPresenterCompl(this, getRequestType(), getProvinceName());
    }

    private String getRequestType() {
        return getIntent().getStringExtra(Constants.RESORT_TYPE);
    }

    private String getProvinceName() {
        return getIntent().getStringExtra(RESORT_BY_PROVINCE);
    }

    // for by province request
    public static void lunch(Context context, String resortType, String provinceName) {
        Intent intent = new Intent(context, ResortActivity.class);
        intent.putExtra(Constants.RESORT_TYPE, resortType);
        intent.putExtra(RESORT_BY_PROVINCE, provinceName);
        context.startActivity(intent);
    }

    public static void lunch(Context context, String resortType) {
        Intent intent = new Intent(context, ResortActivity.class);
        intent.putExtra(Constants.RESORT_TYPE, resortType);
        context.startActivity(intent);
    }
}
