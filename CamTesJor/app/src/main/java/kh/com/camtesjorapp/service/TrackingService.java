package kh.com.camtesjorapp.service;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.os.IBinder;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import kh.com.camtesjorapp.interfaces.LocationUpdaterInterface;
import kh.com.camtesjorapp.model.TrackingModel;
import kh.com.camtesjorapp.model.UserModel;
import kh.com.camtesjorapp.sharepref.UserSharePref;
import kh.com.camtesjorapp.controller.LocationRequestController;

public class TrackingService extends Service implements LocationUpdaterInterface {

    private LocationRequestController mLocationRequestController;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return setUserInfoData(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mLocationRequestController != null)
            mLocationRequestController.removeLocationUpdate();
    }

    @Override
    public void onLocationCallBack(Object type, Location location) {
        setLocation(location.getLatitude(),location.getLongitude());
    }

    private int setUserInfoData(Intent intent) {

        if(mLocationRequestController != null){
            mLocationRequestController.removeLocationUpdate();
            mLocationRequestController = null;
        }

        int priority = intent == null ?
                LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY :
                intent.getIntExtra("priority",0);

        mLocationRequestController = new LocationRequestController(this, priority);
        mLocationRequestController.setLocationUpdateListener(this);
        LatLng latLng = UserSharePref.getInstance(this).getOldLocation(this);
        if(latLng != null) setLocation(latLng.latitude,latLng.longitude);

        return START_STICKY;
    }

    private void setLocation(double lat,double lng) {

        UserModel userModel = UserSharePref.getInstance(this).getUserInfo(this);
        UserSharePref.getInstance(this).setLocation(new LatLng(lat,lng));

        if(userModel == null) return;

        if (userModel.getGroup_admin() != null) {
            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().
                    child("group").child(UserSharePref.getInstance(this).getCurrentCircle()).
                    child(userModel.getUid());

            TrackingModel trackingModel = new TrackingModel();
            trackingModel.setFull_name(userModel.getFull_name());
            trackingModel.setImage_url(userModel.getImage_url());
            trackingModel.setUid(userModel.getUid());
            trackingModel.setLat(lat);
            trackingModel.setLng(lng);
            databaseReference.setValue(trackingModel);

            DatabaseReference timeStampDb = FirebaseDatabase.getInstance().getReference().
                    child("group_battery").child(UserSharePref.getInstance(this).getCurrentCircle()).
                    child(userModel.getUid()).child("time_stamp");

            timeStampDb.setValue(System.currentTimeMillis());
        }
    }
}
