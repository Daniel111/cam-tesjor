package kh.com.camtesjorapp.controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.model.UserTrackingInfo;

public class MarkerMapRender extends DefaultClusterRenderer<UserTrackingInfo>
        implements ClusterManager.OnClusterItemInfoWindowClickListener<UserTrackingInfo> {

    private Context mContext;
    private IconGenerator mIconGenerator;
    private ImageView mImageView;

    public MarkerMapRender(Context context, GoogleMap map, ClusterManager<UserTrackingInfo> clusterManager) {
        super(context, map, clusterManager);

        mContext = context;
        mIconGenerator = new IconGenerator(mContext);
        mImageView = (ImageView) LayoutInflater.from(mContext).inflate(R.layout.map_marker, null, false);
        mIconGenerator.setBackground(null);
        mIconGenerator.setContentView(mImageView);
        clusterManager.setOnClusterItemInfoWindowClickListener(this);
    }

    @Override
    protected void onBeforeClusterItemRendered(UserTrackingInfo item, MarkerOptions markerOptions) {
        // Draw a single person.
        // Set the ic_info window to show their name.
        if (null != item.getmBitmapLogo())
            mImageView.setImageBitmap(item.getmBitmapLogo());
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(mIconGenerator.makeIcon())).title(item.getmName())
                .position(item.getPosition());
    }

    public void updateMarkerPos(UserTrackingInfo item, LatLng latLng, Projection projection){
        if(projection != null) MarkerAnimController.animateMarker(getMarker(item), latLng,projection);
    }

    @Override
    public Marker getMarker(UserTrackingInfo clusterItem) {
        return super.getMarker(clusterItem);
    }

    @Override
    protected void onBeforeClusterRendered(Cluster<UserTrackingInfo> cluster, MarkerOptions markerOptions) {
        super.onBeforeClusterRendered(cluster, markerOptions);
    }

    /**
     * @param cluster
     * @return
     */
    @Override
    protected boolean shouldRenderAsCluster(Cluster cluster) {
        // Always render clusters.
        return false;
    }



    @Override
    public void onClusterItemInfoWindowClick(UserTrackingInfo markerMapModel) {
//        RestaurantDetailActivity.launch(mContext, markerMapModel.getBrandId(), markerMapModel.getId(), -1, null);
    }
}
