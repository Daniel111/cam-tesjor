package kh.com.camtesjorapp.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;

import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.sharepref.UserSharePref;

public class AddMemberDialog extends Dialog {

    public AddMemberDialog(@NonNull Context context) {
        super(context);
        init();
    }

    private void init() {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_create_group);

        if (getWindow() != null) {
            getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            getWindow().setDimAmount(0.5f);
        }
        setCancelable(false);

        final String myCircleId = UserSharePref.getInstance(getContext()).getUserInfo(getContext()).getGroup_admin();
        ((TextView) findViewById(R.id.text_view_invite_code)).setText(myCircleId);

        findViewById(R.id.image_view_dismiss).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        findViewById(R.id.button_done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        findViewById(R.id.button_share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, "This is my circle id " + myCircleId + ". Download the app here " + buildDynamicLink());

                if (shareIntent.resolveActivity(getContext().getPackageManager()) != null)
                    getContext().startActivity(Intent.createChooser(shareIntent, "Share"));
                else
                    getContext().startActivity(shareIntent);
            }
        });

    }

    private String buildDynamicLink(/*String link, String description, String titleSocial, String source*/) {
        //more info at https://firebase.google.com/docs/dynamic-links/create-manually

//        String path = FirebaseDynamicLinks.getInstance().createDynamicLink()
//                .setDynamicLinkDomain("m9guj.app.goo.gl")
//                .setLink(Uri.parse("https://camtesjorapp.page.link/qL6j"))
//                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build()) //com.melardev.tutorialsfirebase
////                .setSocialMetaTagParameters(new DynamicLink.SocialMetaTagParameters.Builder().setTitle("Share this App").setDescription("blabla").build())
////                .setGoogleAnalyticsParameters(new DynamicLink.GoogleAnalyticsParameters.Builder().setSource("AndroidApp").build())
//
//                .buildDynamicLink().getUri().toString();
//
//        return path;

        return "https://m9guj.app.goo.gl/?" +
                "link=" + /*link*/
                "https://camtesjorapp.page.link/qL6j" +
//                "&apn=" + /*getPackageName()*/
//                "com.melardev.tutorialsfirebase" +
//                "&st=" + /*titleSocial*/
//                "Share+this+App" +
//                "&sd=" + /*description*/
//                "looking+to+learn+how+to+use+Firebase+in+Android?+this+app+is+what+you+are+looking+for." +
                "&utm_source=" + /*source*/
                "AndroidApp";
    }
}
