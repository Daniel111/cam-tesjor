package kh.com.camtesjorapp.ui.mygroup;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;

import com.google.android.gms.location.LocationRequest;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import java.util.List;
import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.base.BaseActivity;
import kh.com.camtesjorapp.constant.Constants;
import kh.com.camtesjorapp.dialog.AddMemberDialog;
import kh.com.camtesjorapp.interfaces.LocationUpdaterInterface;
import kh.com.camtesjorapp.model.BatteryModel;
import kh.com.camtesjorapp.service.BatteryService;
import kh.com.camtesjorapp.service.BootReceiver;
import kh.com.camtesjorapp.model.TrackingModel;
import kh.com.camtesjorapp.model.UserModel;
import kh.com.camtesjorapp.mvp.my_group.presenter.IMyGroupPresenter;
import kh.com.camtesjorapp.mvp.my_group.presenter.MyGroupPresenter;
import kh.com.camtesjorapp.mvp.my_group.view.IMyGroupView;
import kh.com.camtesjorapp.service.TrackingService;
import kh.com.camtesjorapp.sharepref.UserSharePref;
import kh.com.camtesjorapp.ui.membercontrol.activity.MemberControlActivity;
import kh.com.camtesjorapp.controller.LocationRequestController;

public class MyGroupActivity extends BaseActivity implements IMyGroupView, LocationUpdaterInterface {

    public static final int REQUEST_CREATE_GROUP = 111;
    public static final String GROUP_ID = "group_name";
    private IMyGroupPresenter mIMyGroupPresenter;
    private boolean isGroupAdmin = false;
    private String mGroupId = "df49afa1-d896-4827-8c9b-482d87550220";
    private LocationRequestController locationRequestController;
    private Location mCurLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_group);
        mIMyGroupPresenter = new MyGroupPresenter(this);
        mIMyGroupPresenter.onCheckUserSignIn();
    }

    @Override
    protected String getToolbarTitle() {
        return "My Group";
    }

    @Override
    public boolean isDisplaySearchBar() {
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_CREATE_GROUP) {
            isGroupAdmin = true;
            mIMyGroupPresenter.onGetUserDataFromCurrentGroup(data.getStringExtra(GROUP_ID));
        } else if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_SIGN_IN_CODE) {
            init();
        } else if (resultCode == RESULT_CANCELED && requestCode == Constants.REQUEST_SIGN_IN_CODE) {
            finish();
        }
    }

//    @Override
//    public void onUserAlreadySignIn() {
//        init();
//    }
//
//    @Override
//    public void onUserNotYetSignIn() {
//        SignInActivity.lunch(MyGroupActivity.this);
//    }

    @Override
    public void onGetMembersDataSucceed(List<TrackingModel> trackingModels, List<BatteryModel> batteryModels) {

        UserSharePref userSharePref = UserSharePref.getInstance(MyGroupActivity.this);
        UserModel userModel = userSharePref.getUserInfo(MyGroupActivity.this);

        if (isGroupAdmin) userModel.setGroup_admin(mGroupId);
        else userModel.setGroup_joining(mGroupId);

        FirebaseDatabase.getInstance().getReference().child("users").
                child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(userModel);

        userSharePref.setUserInfo(userModel);

        String trackingData = new Gson().toJson(trackingModels);
        String batteryData = new Gson().toJson(batteryModels);
        MemberControlActivity.lunch(MyGroupActivity.this, mGroupId, trackingData, batteryData);
    }

    @Override
    public void onGetMembersDataCancel(DatabaseError databaseError) {

    }

    @Override
    public void onLocationCallBack(Object type, Location location) {
        locationRequestController.removeLocationUpdate();
        locationRequestController = null;
        mCurLocation = location;
    }


    private void init() {

        locationRequestController = new LocationRequestController(this, LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequestController.setLocationUpdateListener(this);

        FloatingActionButton createGroup = findViewById(R.id.create_admin_group);
        createGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AddMemberDialog(MyGroupActivity.this).show();
            }
        });
        FloatingActionButton joinGroup = findViewById(R.id.join_group);
        joinGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isGroupAdmin = false;
                startServices();
                mIMyGroupPresenter.onGetUserDataFromCurrentGroup(mGroupId);
            }
        });
    }

    public static void lunch(Context context) {
        Intent intent = new Intent(context, MyGroupActivity.class);
        context.startActivity(intent);
    }

    private void startServices() {

        //call Tracking service
        Intent trackingIntent = new Intent(MyGroupActivity.this, TrackingService.class);
        if (mCurLocation != null) {
            trackingIntent.putExtra("lat", mCurLocation.getLatitude());
            trackingIntent.putExtra("long", mCurLocation.getLongitude());
        }
        startService(trackingIntent);

        //call battery service
        Intent batteryIntent = new Intent(MyGroupActivity.this, BatteryService.class);
        batteryIntent.putExtra(BootReceiver.ACTION_BOOT, BootReceiver.ACTION_BOOT);
        startService(batteryIntent);
    }
}
