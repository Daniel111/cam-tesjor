package kh.com.camtesjorapp.ui.resortdetail.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import com.daimajia.slider.library.SliderLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.constant.Constants;
import kh.com.camtesjorapp.helper.GlideSliderViewHelper;
import kh.com.camtesjorapp.base.CamTesjorActivity;
import kh.com.camtesjorapp.ui.resortdetail.adapter.ResortDetailAdapter;
import kh.com.camtesjorapp.ui.resortdetail.presenter.IResortDetailPresenter;
import kh.com.camtesjorapp.ui.resortdetail.presenter.ResortDetailPresenter;
import kh.com.camtesjorapp.ui.resortdetail.view.IResortDetailView;

public class ResortDetailActivity extends CamTesjorActivity implements IResortDetailView {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarColorToTrans();
        setContentView(R.layout.activity_resort_detail);
        init();
    }

    private void setStatusBarColorToTrans(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(getResources().getColor(android.R.color.transparent));
        }
    }

    private void init(){
        ResortDetailAdapter mResortDetailAdapter = new ResortDetailAdapter(getSupportFragmentManager());
        ViewPager mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mResortDetailAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle(getIntent().getStringExtra(Constants.RESORT_NAME));
        }

        mToolbar.setNavigationIcon(R.drawable.ic_back_left_arrow);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        IResortDetailPresenter mIResortDetailPresenter = new
                ResortDetailPresenter(this,getIntent().getStringExtra(Constants.RESORT_DETAIL_KEY));
        mIResortDetailPresenter.retrieveData();
    }

    public static void lunch(Context context, String resortDetailKey,String resortName){
        Intent intent = new Intent(context, ResortDetailActivity.class);
        intent.putExtra(Constants.RESORT_DETAIL_KEY,resortDetailKey);
        intent.putExtra(Constants.RESORT_NAME,resortName);
        context.startActivity(intent);
    }

    @Override
    public void onRetrieveDataSucceed(DataSnapshot snapshot) {

        SliderLayout mSliderLayout = (SliderLayout)findViewById(R.id.vp_resort_detail);

        if(snapshot != null){

            GlideSliderViewHelper defaultSliderView = new GlideSliderViewHelper(this);
            defaultSliderView.image("https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg");
            mSliderLayout.addSlider(defaultSliderView);
            mSliderLayout.stopAutoCycle();


//            for(DataSnapshot dataSnapshot:snapshot.child("images").getChildren()){
//                GlideSliderViewHelper defaultSliderView = new GlideSliderViewHelper(this);
//                defaultSliderView.image(dataSnapshot.getValue().toString());
//                mSliderLayout.addSlider(defaultSliderView);
//            }
//            mSliderLayout.stopAutoCycle();
//            mSliderLayout.setDuration(5000);
        }
    }

    @Override
    public void onRetrieveDataError(DatabaseError databaseError) {

    }
}
