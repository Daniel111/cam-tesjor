package kh.com.camtesjorapp.ui.province.activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import java.util.ArrayList;
import java.util.List;
import kh.com.camtesjorapp.ui.province.adapter.ProvinceAdapter;
import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.base.BaseActivity;
import kh.com.camtesjorapp.model.HomeScreenModel;
import kh.com.camtesjorapp.ui.province.presenter.ProvincePresenterCompl;
import kh.com.camtesjorapp.ui.province.presenter.IProvincePresenter;
import kh.com.camtesjorapp.ui.province.view.IProvinceView;
import kh.com.camtesjorapp.ui.resort.activity.ResortActivity;

public class ProvinceActivity extends BaseActivity implements IProvinceView,ProvinceAdapter.OnProvinceItemClick {

    private ProgressBar progressBar;
    private List<HomeScreenModel> homeScreenModels = new ArrayList<>();
    private ProvinceAdapter provinceAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_province);
        IProvincePresenter mIProvincePresenter = new ProvincePresenterCompl(this);
        mIProvincePresenter.retrieveData();
        init();
    }

    @Override
    public void onRetrieveDataSucceed(DataSnapshot snapshot) {
        progressBar.setVisibility(View.GONE);
        for(DataSnapshot dataSnapshot : snapshot.getChildren()){

            HomeScreenModel homeScreenModel = new HomeScreenModel();
            homeScreenModel.setmProvinceName(dataSnapshot.child("name").getValue().toString());
            homeScreenModel.setmImageUrl(dataSnapshot.child("image_url").getValue().toString());
            homeScreenModel.setmResortKey(dataSnapshot.child("resort_name").getValue().toString());
            homeScreenModels.add(homeScreenModel);
        }
        provinceAdapter.notifyDataSetChanged();
    }

    @Override
    public void onRetrieveDataError(DatabaseError databaseError) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
                findViewById(R.id.txt_no_data).setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    protected String getToolbarTitle() {
//       return getResources().getString(R.string.provinces);
        return "";
    }

    @Override
    public void onClicked(String provinceName) {
        progressBar.setVisibility(View.GONE);
        ResortActivity.lunch(this,ResortActivity.RESORT_BY_PROVINCE,provinceName);
    }

    private void init(){
        progressBar = (ProgressBar) findViewById(R.id.province_progress_bar);
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.rec_province);
        provinceAdapter = new ProvinceAdapter(this,homeScreenModels);
        GridLayoutManager layoutManager = new GridLayoutManager(this,2,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(provinceAdapter);
    }

    public static void lunch(Context context){
        Intent placesScreen = new Intent(context, ProvinceActivity.class);
        context.startActivity(placesScreen);
    }

}








