package kh.com.camtesjorapp.ui.province.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.ui.province.activity.ProvinceActivity;
import kh.com.camtesjorapp.model.HomeScreenModel;

public class ProvinceAdapter extends RecyclerView.Adapter<ProvinceAdapter.ViewHolder> {

    public interface OnProvinceItemClick{
        void onClicked(String provinceName);
    }

    private Context mContext;
    private List<HomeScreenModel> mHomeScreenModels;
    private OnProvinceItemClick mOnProvinceItemClick;

    public ProvinceAdapter(Context context,List<HomeScreenModel> homeScreenModels) {
        this.mContext = context;
        this.mOnProvinceItemClick = (ProvinceActivity)context;
        this.mHomeScreenModels = homeScreenModels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.provinces_item, parent, false);
        return new ProvinceAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.provinceName.setText(mHomeScreenModels.get(position).getmProvinceName());
        Glide.with(mContext).load(mHomeScreenModels.get(position).getmImageUrl()).into(holder.provinceImage);
    }

    @Override
    public int getItemCount() {
        return (mHomeScreenModels!=null)?mHomeScreenModels.size():0;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView provinceImage;
        private TextView provinceName;

        public ViewHolder(View itemView) {
            super(itemView);
            provinceImage = (ImageView)itemView.findViewById(R.id.img_province);
            provinceName = (TextView)itemView.findViewById(R.id.tv_province_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(mOnProvinceItemClick != null){
                mOnProvinceItemClick.onClicked(mHomeScreenModels.get(getAdapterPosition()).getmResortKey());
            }
        }
    }
}
