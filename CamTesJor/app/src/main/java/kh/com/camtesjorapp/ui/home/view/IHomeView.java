package kh.com.camtesjorapp.ui.home.view;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

/**
 * Created by mac on 5/24/2017 AD.
 */

public interface IHomeView {
    void onRetrieveDataSucceed(DataSnapshot snapshot);
//    void onSetProgressBarVisibility(int visibility);
    void onRetrieveDataError(DatabaseError databaseError);
}
