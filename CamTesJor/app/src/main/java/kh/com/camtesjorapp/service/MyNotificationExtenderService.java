package kh.com.camtesjorapp.service;

import android.content.Intent;
import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotificationReceivedResult;
import org.json.JSONObject;
import kh.com.camtesjorapp.constant.IntentKeys;

public class MyNotificationExtenderService extends NotificationExtenderService {

    @Override
    protected boolean onNotificationProcessing(OSNotificationReceivedResult notification) {

        onBroadcastEmitResult(notification.payload.toJSONObject());

//        OverrideSettings overrideSettings = new OverrideSettings();
//        overrideSettings.extender = new NotificationCompat.Extender() {
//            @Override
//            public NotificationCompat.Builder extend(NotificationCompat.Builder builder) {
//                // Sets the background notification color to Red on Android 5.0+ devices.
//                Bitmap icon = BitmapFactory.decodeResource(CamTesjorApp.getContext().getResources(),
//                        R.drawable.ic_add);
//                builder.setLargeIcon(icon);
//                return builder.setColor(new BigInteger("FF0000FF", 16).intValue());
//            }
//        };
        return false;
    }

    private void onBroadcastEmitResult(/*String event, */JSONObject dataNotification) {

        Intent intent = new Intent(IntentKeys.EVENT_EMIT_RESULT);
//        intent.putExtra(IntentKeys.EXTRA_EMIT_KEY_RESULT, event);
        intent.putExtra(IntentKeys.EXTRA_DATA, dataNotification.toString());
        sendBroadcast(intent);
        intent = null;
    }

    public void sendBroadcast(Intent intent) {
        if (intent != null) {
            super.sendBroadcast(intent);
            intent = null;
        }
    }
}
