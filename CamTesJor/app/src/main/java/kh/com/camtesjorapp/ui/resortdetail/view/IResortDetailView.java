package kh.com.camtesjorapp.ui.resortdetail.view;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

/**
 * Created by mac on 6/8/2017 AD.
 */

public interface IResortDetailView {

    void onRetrieveDataSucceed(DataSnapshot snapshot);
    //    void onSetProgressBarVisibility(int visibility);
    void onRetrieveDataError(DatabaseError databaseError);
}
