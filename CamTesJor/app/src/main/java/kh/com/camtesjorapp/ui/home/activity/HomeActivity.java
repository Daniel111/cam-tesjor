package kh.com.camtesjorapp.ui.home.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import java.util.ArrayList;
import java.util.List;
import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.constant.Constants;
import kh.com.camtesjorapp.custom.CustomFrameLayout;
import kh.com.camtesjorapp.base.CamTesjorActivity;
import kh.com.camtesjorapp.model.UserModel;
import kh.com.camtesjorapp.service.BatteryService;
import kh.com.camtesjorapp.ui.controlmember.ControlMemberActivity;
import kh.com.camtesjorapp.ui.mygroup.MyGroupActivity;
import kh.com.camtesjorapp.service.TrackingService;
import kh.com.camtesjorapp.sharepref.UserSharePref;
import kh.com.camtesjorapp.model.WallPaperModel;
import kh.com.camtesjorapp.ui.home.presenter.HomePresenterCompl;
import kh.com.camtesjorapp.ui.home.view.IHomeView;
import kh.com.camtesjorapp.ui.membercontrol.activity.MemberControlActivity;
import kh.com.camtesjorapp.ui.profile.ProfileActivity;
import kh.com.camtesjorapp.ui.province.activity.ProvinceActivity;
import kh.com.camtesjorapp.ui.resort.activity.ResortActivity;

public class HomeActivity extends CamTesjorActivity implements IHomeView, View.OnClickListener
        , NavigationView.OnNavigationItemSelectedListener {

    private CustomFrameLayout camTesjorSlider;
    private DrawerLayout drawer;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        init();

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_go_to_places_now:
                ProvinceActivity.lunch(this);
                break;
            case R.id.btn_all_province:
                ResortActivity.lunch(this, ResortActivity.ALL_RESORT);
                break;
            case R.id.btn_drawer:
                drawer.openDrawer(GravityCompat.START);
                break;
            case R.id.btn_near_by:
                MemberControlActivity.lunch(this);
                break;
            case R.id.btn_popular:
                ResortActivity.lunch(this, ResortActivity.POPULAR_RESORT);
                break;
            case R.id.member_control:
//                MyGroupActivity.lunch(this);
                ControlMemberActivity.lunch(this);
                break;
            case R.id.user_img:
                ProfileActivity.lunch(this);

        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_log_out) {
            UserSharePref.getInstance(this).resetUserModel();
            setUserInfo();
            firebaseAuth.signOut();
            LoginManager.getInstance().logOut();
            stopService(new Intent(this,TrackingService.class));
            stopService(new Intent(this,BatteryService.class));
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        camTesjorSlider.pause();

    }

    @Override
    protected void onResume() {
        super.onResume();
        camTesjorSlider.start();
        setUserInfo();
    }

    @Override
    protected void onStart() {
        super.onStart();
        camTesjorSlider.start();
    }

    @Override
    public void onRetrieveDataSucceed(DataSnapshot snapshot) {

        List<WallPaperModel> adsList = new ArrayList<>();

        for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
            WallPaperModel wallPaperModel = new WallPaperModel();
            wallPaperModel.setBabFilePath(dataSnapshot.getValue().toString());
            adsList.add(wallPaperModel);
        }

        UserSharePref.getInstance(this).storeBannerData(adsList);
        CustomFrameLayout.refresh();
    }

    @Override
    public void onRetrieveDataError(DatabaseError databaseError) {
    }

    private void init() {

        firebaseAuth = FirebaseAuth.getInstance();
        new HomePresenterCompl(this, Constants.MAIN_SCREEN_PATH).retrieveData();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        camTesjorSlider = findViewById(R.id.main_screen_viewpager);
        Button mBtnGoToPlaceNow = findViewById(R.id.btn_go_to_places_now);
        findViewById(R.id.btn_drawer).setOnClickListener(this);
        mBtnGoToPlaceNow.setOnClickListener(this);
        findViewById(R.id.btn_near_by).setOnClickListener(this);
        findViewById(R.id.btn_all_province).setOnClickListener(this);
        findViewById(R.id.btn_popular).setOnClickListener(this);
        findViewById(R.id.member_control).setOnClickListener(this);
    }

    private void setUserInfo() {
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View navHeader = navigationView.getHeaderView(0);
        ImageView userImage = navHeader.findViewById(R.id.user_img);
        userImage.setOnClickListener(this);
        UserModel userModel = UserSharePref.getInstance(this).getUserInfo(this);
        if (userModel != null) {
            Glide.with(this).load(userModel.getImage_url()).
                    error(R.drawable.com_facebook_profile_picture_blank_square).into(userImage);
            ((TextView) navHeader.findViewById(R.id.text_view_user_name)).setText(userModel.getFull_name());
        } else {
            userImage.setImageDrawable(getResources().getDrawable(R.drawable.com_facebook_profile_picture_blank_square));
            ((TextView) navHeader.findViewById(R.id.text_view_user_name)).setText(getResources().getString(R.string.app_name));
        }
    }

    public static void lunch(Context context) {
        Intent intent = new Intent(context, HomeActivity.class);
        context.startActivity(intent);
        ((Activity) context).finish();
    }
}
