package kh.com.camtesjorapp.mvp.member.view;

import com.google.firebase.database.DatabaseError;

import java.util.List;

import kh.com.camtesjorapp.model.BatteryModel;
import kh.com.camtesjorapp.model.TrackingModel;

/**
 * Created by mac on 5/25/2018 AD.
 */

public interface IMemberView {

    void onGetMembersDataFromGroupJoiningSucceed(List<TrackingModel> trackingModel);
    void onGetMembersDataFromGroupAdminSucceed(List<TrackingModel> trackingModel);
    void onGetMembersDataCancel(DatabaseError databaseError);
}
