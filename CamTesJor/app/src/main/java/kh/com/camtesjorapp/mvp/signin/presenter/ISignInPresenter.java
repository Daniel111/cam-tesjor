package kh.com.camtesjorapp.mvp.signin.presenter;

import com.facebook.CallbackManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

public interface ISignInPresenter {

    void retrieveData();
    void onSignInFacebook(LoginButton loginButton, CallbackManager callbackManager);
    void onEmailSignIn(String email,String pass);
    void onTokenSignIn(String token);
    void onFbDataRequest(LoginResult loginResult);
    void onFireBaseUserDataRequest();
    void onGplusSignIn();
}
