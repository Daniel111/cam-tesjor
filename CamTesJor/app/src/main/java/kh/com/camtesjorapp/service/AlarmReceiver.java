package kh.com.camtesjorapp.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.format.DateUtils;

import com.google.android.gms.location.LocationRequest;

public class AlarmReceiver extends BroadcastReceiver {

    private static final int REQUEST_CODE = 777;
    private static long ALARM_INTERVAL = DateUtils.MINUTE_IN_MILLIS * 2 ;

    @Override
    public void onReceive(final Context context, final Intent intent) {

        if (context == null) {
            // Somehow you've lost your context; this really shouldn't happen
            return;
        }
        if (intent == null) {
            // No intent was passed to your receiver; this also really shouldn't happen
            return;
        }
        if (intent.getAction() == null) {
            startService(context, LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        }
    }

    public static void startAlarms(final Context context) {

        startService(context,LocationRequest.PRIORITY_HIGH_ACCURACY);

        final AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        // start alarm right away
        if (manager != null) {

            manager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 0, ALARM_INTERVAL,
                    getAlarmIntent(context));

        }
    }

    private static PendingIntent getAlarmIntent(final Context context) {
        return PendingIntent.getBroadcast(context, REQUEST_CODE, new Intent(context, AlarmReceiver.class), PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private static void startService(Context context,int priority){

        // If you called your Receiver explicitly, this is what you should expect to happen
        Intent monitorIntent = new Intent(context, BatteryService.class);
        monitorIntent.putExtra(BatteryService.BATTERY_UPDATE, true);
        context.startService(monitorIntent);

        Intent intent = new Intent(context,TrackingService.class);
        intent.putExtra("priority",priority);
        context.startService(intent);
    }
}
