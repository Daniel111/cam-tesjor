package kh.com.camtesjorapp.constant;

/**
 * Created by mac on 3/1/2018 AD.
 */

public interface IntentKeys {

    String PERMISSION_ADMIN = "com.socket.permission.ADMINISTRATOR";

    String ACTION_REFRESH_CONNECTION_STATE = "com.socket.action.ACTION_REFRESH_CONNECTION_STATE";
    String ACTION_EMIT = "com.socket.action.ACTION_EMIT";
    String ACTION_CANCEL_EMIT = "com.socket.action.ACTION_CANCEL_EMIT";
    String ACTION_SET_BADGE = "com.socket.action.ACTION_SET_BADGE";
    String ACTION_SERVICE_BROADCAST_RECEIVER_STATE = "com.socket.action.ACTION_SERVICE_BROADCAST_RECEIVER_STATE";
    String ACTION_ON = "com.socket.action.ACTION_ON_EMITTER_LISTENER";
    String ACTION_OFF = "com.socket.action.ACTION_OFF_EMITTER_LISTENER";
    String ACTION_CONNECT = "com.socket.action.ACTION_CONNECT";
    String ACTION_DISCONNECT = "com.socket.action.ACTION_DISCONNECT";

    String EVENT_ON_CONNECTION_CHANGE = "com.socket.event.EVENT_ON_CONNECTION_CHANGE";
    String EVENT_EMIT_RESULT = "com.socket.event.EVENT_EMIT_RESULT";
    String EVENT_SERVER_PROBLEM = "com.socket.event.EVENT_SERVER_PROBLEM";
    String EVENT_MESSAGE = "com.socket.event.EVENT_MESSAGE";
    String EVENT_PAYMENT_COMPLETE = "PAYMENTCOMPLETE";
    String EVENT_BOOKING = "BOOKING";
    String EVENT_REDEEM = "REDEEM";
    String EVENT_REJECT = "REJECT";
    String EVENT_APPROVE = "APPROVE";
    String EVENT_AUTO_CANCEL = "AUTOCANCEL";
    String EVENT_CANCEL = "CANCEL";
    String EVENT_COUPON = "COUPON";
    String EVENT_CHAT = "CHAT";
    String EVENT_INVITE_ORDER = "INVITE_ORDER";
    String EVENT_JOIN_ORDER = "JOIN_ORDER";
    String EVENT_REJECT_ORDER = "REJECT_ORDER";
    String EVENT_ACCEPT_ORDER = "ACCEPT_ORDER";
    String EVENT_DENY_ORDER = "DENY_ORDER";
    String EVENT_REQUEST_ORDER = "REQUEST_ORDER";
    String EVENT_KICK_ORDER = "KICK_ORDER";
    String EVENT_KICK_ORDER_SELF = "KICK_ORDER_SELF";
    String EVENT_ANNOUNCEMENT_CLIENT_ANDROID = "ANNOUNCEMENT_CLIENT_ANDROID";
    String EVENT_REDEEM_ITEM = "REDEEM_ITEM";
    String EVENT_CHAT_NAME_UPDATE = "CHAT_NAME_UPDATE";
    String EVENT_SEND_SUGGEST = "SEND_SUGGEST";
    String EVENT_ACCEPT_SUGGEST = "ACCEPT_SUGGEST";

    String EVENT_NOTIFICATION = "com.socket.event.EVENT_NOTIFICATION";
    String EXTRA_CONNECTION_STATE = "com.socket.extra.EXTRA_CONNECTION_STATE";
    String EXTRA_SOCKET_CONNECTION_STATE = "com.socket.extra.EXTRA_SOCKET_CONNECTION_STATE";
    String EXTRA_BROADCAST_RECEIVER_STATE = "com.socket.extra.EXTRA_BROADCAST_RECEIVER_STATE";
    String EXTRA_EMIT_FUNCTION = "com.socket.extra.EMIT_FUNCTION";
    String EXTRA_EMIT_DATA = "com.socket.extra.EMIT_DATA";
    String EXTRA_EMIT_KEY_RESULT = "com.socket.extra.EMIT_KEY";
    String EXTRA_DATA = "com.socket.extra.EXTRA_DATA";
    String EXTRA_BADGE_NUMBER = "com.socket.extra.BADGE_NUMBER";
    String EXTRA_BADGE_NAME = "com.socket.extra.BADGE_NAME";


    int CONNECTED = 1;
    int NO_SOCKET_CONNECTION = 0;
    int CONNECTING = 2;
}
