package kh.com.camtesjorapp.ui.resortdetail.presenter;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import kh.com.camtesjorapp.constant.Constants;
import kh.com.camtesjorapp.ui.resortdetail.view.IResortDetailView;

/**
 * Created by mac on 6/8/2017 AD.
 */

public class ResortDetailPresenter implements IResortDetailPresenter {

    private IResortDetailView mIResortDetailView;
    private String mResortDetailKey;

    public ResortDetailPresenter(IResortDetailView iResortDetailView,String resortDetailKey){
        this.mIResortDetailView = iResortDetailView;
        this.mResortDetailKey = resortDetailKey;
    }

    @Override
    public void retrieveData() {

        DatabaseReference resort_detail = FirebaseDatabase.
                getInstance().
                getReference().
                child(Constants.RESORT_DETAIL_PATH).
                child(mResortDetailKey);

        resort_detail.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mIResortDetailView.onRetrieveDataSucceed(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mIResortDetailView.onRetrieveDataError(databaseError);
            }
        });
    }
}
