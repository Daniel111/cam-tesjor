package kh.com.camtesjorapp.ui.signin;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.facebook.CallbackManager;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;
import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.base.BaseActivity;
import kh.com.camtesjorapp.constant.Constants;
import kh.com.camtesjorapp.model.UserModel;
import kh.com.camtesjorapp.mvp.signin.presenter.SignInPresenter;
import kh.com.camtesjorapp.mvp.signin.view.ISignInVIew;
import kh.com.camtesjorapp.sharepref.UserSharePref;
import kh.com.camtesjorapp.ui.home.activity.HomeActivity;
import kh.com.camtesjorapp.ui.signup.SignUpActivity;
import kh.com.camtesjorapp.utils.Utils;

public class SignInActivity extends BaseActivity implements View.OnClickListener,ISignInVIew {

    private EditText email;
    private EditText password;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;
    private ProgressDialog progressDialog;
    private CallbackManager mCallbackManager;
    private LoginButton mLoginButton;
    private SignInPresenter mSignInPresenter;
    private JSONObject mFacebookData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        init();
    }

    @Override
    public boolean isDisplaySearchBar() {
        return false;
    }

    @Override
    protected String getStringToolbarTitle() {
        return "Sign In";
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && requestCode == SignUpActivity.REGISTER_REQUEST_CODE){
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_register:
                SignUpActivity.lunch(this);
                break;
            case R.id.sign_in:
                signInWithEmailAndPass();
                break;
            case R.id.image_view_facebook_sign_in:
                signInWithFacebook();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_CANCELED);
    }

    @Override
    public void onRetrieveDataSucceed(DataSnapshot snapshot) {

    }

    @Override
    public void onRetrieveDataError(DatabaseError databaseError) {

    }

    @Override
    public void onFacebookSignInSucceed(LoginResult loginResult) {
        mSignInPresenter.onFbDataRequest(loginResult);
    }

    @Override
    public void onFacebookSignInFailed(FacebookException error) {
        progressDialog.dismiss();
    }

    @Override
    public void onFacebookSignInCanceled() {
        progressDialog.dismiss();
    }

    @Override
    public void onTokenSignInSucceed(Task<AuthResult> task) {
        getFacebookData(mFacebookData);
    }

    @Override
    public void onTokenSignInFailed(Task<AuthResult> task) {
        Toast.makeText(SignInActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
        progressDialog.dismiss();
    }

    @Override
    public void onEmailSignInSucceed(Task<AuthResult> task) {
        mSignInPresenter.onFireBaseUserDataRequest();
    }

    @Override
    public void onEmailSignInFailed(Task<AuthResult> task) {
        Toast.makeText(SignInActivity.this, "Incorrect password or email.", Toast.LENGTH_SHORT).show();
        progressDialog.dismiss();
    }

    @Override
    public void onFbDataRespond(JSONObject jsonObject,LoginResult loginResult) {
        mSignInPresenter.onTokenSignIn(loginResult.getAccessToken().getToken());
        mFacebookData = jsonObject;
    }

    @Override
    public void onFirebaseUserDataRespond(DataSnapshot dataSnapshot) {
        UserModel userModel = dataSnapshot.getValue(UserModel.class);
        UserSharePref.getInstance(this).setUserInfo(userModel);
        OneSignal.sendTag("User_ID",userModel.getUid());
        setResult(RESULT_OK);
        finish();
        progressDialog.dismiss();
    }

    private void init() {

        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("users");
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Signing in...");
        mCallbackManager = CallbackManager.Factory.create();
        mSignInPresenter = new SignInPresenter(this);
        findViewById(R.id.btn_register).setOnClickListener(this);
        findViewById(R.id.sign_in).setOnClickListener(this);
        findViewById(R.id.image_view_facebook_sign_in).setOnClickListener(this);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        mLoginButton = findViewById(R.id.log_in_facebook);
    }

    private void signInWithEmailAndPass() {
        if(TextUtils.isEmpty(Utils.getText(email)) && TextUtils.isEmpty(Utils.getText(password)))
            Toast.makeText(SignInActivity.this, "No username or password entered", Toast.LENGTH_SHORT).show();
        else {
            progressDialog.show();
            mSignInPresenter.onEmailSignIn(Utils.getText(email), Utils.getText(password));}
    }

    private void signInWithFacebook(){
        mSignInPresenter.onSignInFacebook(mLoginButton,mCallbackManager);
        mLoginButton.performClick();
        progressDialog.show();
    }

    private void getFacebookData(JSONObject object) {

        UserModel userModel = new UserModel();

        try {
            userModel.setEmail(object.getString("email"));
        } catch (JSONException e) {
            e.printStackTrace();
            userModel.setEmail(null);
        }

        try {
            String[] name = object.getString("name").split(" ");
            userModel.setFirst_name(name[0]);
            userModel.setLast_name(name[1]);
        } catch (JSONException e) {
            e.printStackTrace();
            userModel.setFirst_name(null);
            userModel.setLast_name(null);
        }

        try {
            userModel.setFull_name(object.getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
            userModel.setFull_name(null);
        }

        try {
            String imageUrl = "https://graph.facebook.com/" + object.getString("id")+ "/picture?type=large";
            userModel.setImage_url(imageUrl);
        } catch (JSONException e) {
            e.printStackTrace();
            userModel.setImage_url(null);
        }

        userModel.setUid(firebaseAuth.getCurrentUser().getUid());
        UserSharePref.getInstance(SignInActivity.this).setUserInfo(userModel);
        DatabaseReference currentDatabase = databaseReference.child(firebaseAuth.getCurrentUser().getUid());
        currentDatabase.setValue(userModel);

        OneSignal.sendTag("User_ID",firebaseAuth.getCurrentUser().getUid());

        setResult(RESULT_OK);
        finish();
        progressDialog.dismiss();
    }

    public static void lunch(Context context) {
        Intent intent = new Intent(context, SignInActivity.class);
        ((Activity) context).startActivityForResult(intent, Constants.REQUEST_SIGN_IN_CODE);
    }
}
