package kh.com.camtesjorapp.mvp.my_group.presenter;

import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import kh.com.camtesjorapp.model.BatteryModel;
import kh.com.camtesjorapp.model.TrackingModel;
import kh.com.camtesjorapp.mvp.my_group.view.ICheckSignInView;
import kh.com.camtesjorapp.mvp.my_group.view.IMyGroupView;
import kh.com.camtesjorapp.mvp.signup.view.ISignUpView;

public class MyGroupPresenter implements IMyGroupPresenter {

    private IMyGroupView mMyGroupView;
    private ICheckSignInView mICheckSignInView;

    public MyGroupPresenter(IMyGroupView myGroupView) {
        this.mMyGroupView = myGroupView;
    }

    public MyGroupPresenter(ICheckSignInView iCheckSignInView) {
        this.mICheckSignInView = iCheckSignInView;
    }

    @Override
    public void onCheckUserSignIn() {

        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuth.addAuthStateListener(new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() == null)
                    mICheckSignInView.onUserNotYetSignIn();
                else
                    mICheckSignInView.onUserAlreadySignIn();

                firebaseAuth.removeAuthStateListener(this);
            }
        });
    }

    @Override
    public void onGetUserDataFromCurrentGroup(final String groupId) {

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("group").child(groupId);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                List<TrackingModel> trackingModels = new ArrayList<>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    TrackingModel trackingModel = dataSnapshot1.getValue(TrackingModel.class);
                    trackingModels.add(trackingModel);
                }
                databaseReference.removeEventListener(this);

                getBatteryData(trackingModels,groupId);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mMyGroupView.onGetMembersDataCancel(databaseError);
                databaseReference.removeEventListener(this);
            }
        });
    }

    private void getBatteryData(final List<TrackingModel> trackingModels, final String groupId){

        final DatabaseReference dbGroupBattery = FirebaseDatabase.getInstance().getReference().child("group_battery").child(groupId);
        dbGroupBattery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                List<BatteryModel> batteryModels = new ArrayList<>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    BatteryModel batteryModel = dataSnapshot1.getValue(BatteryModel.class);
                    batteryModels.add(batteryModel);
                }

                mMyGroupView.onGetMembersDataSucceed(trackingModels,batteryModels);
                dbGroupBattery.removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mMyGroupView.onGetMembersDataCancel(databaseError);
                dbGroupBattery.removeEventListener(this);
            }
        });
    }
}
