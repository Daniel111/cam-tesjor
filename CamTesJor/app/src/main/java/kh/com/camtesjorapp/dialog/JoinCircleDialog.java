package kh.com.camtesjorapp.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.location.LocationRequest;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Scanner;

import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.controller.LocationRequestController;
import kh.com.camtesjorapp.interfaces.LocationUpdaterInterface;
import kh.com.camtesjorapp.model.BatteryModel;
import kh.com.camtesjorapp.model.TrackingModel;
import kh.com.camtesjorapp.model.UserModel;
import kh.com.camtesjorapp.mvp.my_group.presenter.MyGroupPresenter;
import kh.com.camtesjorapp.mvp.my_group.view.IMyGroupView;
import kh.com.camtesjorapp.sharepref.UserSharePref;
import kh.com.camtesjorapp.ui.controlmember.ControlMemberActivity;

public class JoinCircleDialog extends Dialog {

    private ControlMemberActivity mActivity;
    private String currentCircleId;

    public JoinCircleDialog(@NonNull Context context) {
        super(context);
        mActivity = (ControlMemberActivity) context;
        init();
    }

    private void init() {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_join_circle);

        if (getWindow() != null) {
            getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            getWindow().setDimAmount(0.5f);
        }
        setCancelable(false);

        findViewById(R.id.image_view_dismiss).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        findViewById(R.id.button_join_group).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                joinGroup();
            }
        });
        findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    private void joinGroup(){

        currentCircleId = ((EditText)findViewById(R.id.edit_text_group_id)).getText().toString().trim();

        if(currentCircleId.isEmpty()){
            Toast.makeText(getContext(),"Please input circle id",Toast.LENGTH_SHORT).show();
            return;
        }

        mActivity.showProgress();

        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        final DatabaseReference groupIdRef = rootRef.child("group").child(currentCircleId);

        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()) Toast.makeText(getContext(),"Circle id doesn't exist",Toast.LENGTH_SHORT).show();
                else onCircleIdExist();
                groupIdRef.removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                groupIdRef.removeEventListener(this);
            }
        };
        groupIdRef.addListenerForSingleValueEvent(eventListener);
    }

    private void onCircleIdExist(){

        final LocationRequestController locationRequestController = new LocationRequestController(mActivity, LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequestController.setLocationUpdateListener(new LocationUpdaterInterface() {
            @Override
            public void onLocationCallBack(Object type, Location location) {
                setLocation(location);
                locationRequestController.removeLocationUpdate();
            }
        });
        locationRequestController.checkPermissions();

    }

    private void setLocation(Location location) {

        final UserModel userModel = UserSharePref.getInstance(mActivity).getUserInfo(mActivity);

        if(userModel == null) return;

         DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().
                    child("group").child(currentCircleId).
                    child(FirebaseAuth.getInstance().getCurrentUser().getUid());

            TrackingModel trackingModel = new TrackingModel();
            trackingModel.setFull_name(userModel.getFull_name());
            trackingModel.setImage_url(userModel.getImage_url());
            trackingModel.setUid(FirebaseAuth.getInstance().getCurrentUser().getUid());
            trackingModel.setLat(location.getLatitude());
            trackingModel.setLng(location.getLongitude());
            databaseReference.setValue(trackingModel, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    new BatteryCheckAsync().execute();
                }
            });
    }

    @SuppressLint("StaticFieldLeak")
    private class BatteryCheckAsync extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... arg0) {
            //Battery State check - create log entries of current battery state
            IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            Intent batteryStatus = mActivity.registerReceiver(null, ifilter);

            int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                    status == BatteryManager.BATTERY_STATUS_FULL;

            int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

            final UserModel userModel = UserSharePref.getInstance(mActivity).getUserInfo(mActivity);

            if(userModel != null){

                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().
                        child("group_battery").child(currentCircleId).
                        child(FirebaseAuth.getInstance().getCurrentUser().getUid());

                Long tsLong = System.currentTimeMillis();

                BatteryModel batteryModel = new BatteryModel();
                batteryModel.setCharging(isCharging);
                batteryModel.setLevel(level);
                batteryModel.setScale(scale);
                batteryModel.setTime_stamp(tsLong);

                databaseReference.setValue(batteryModel, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                        mActivity.hideProgress();
                        dismiss();

                        UserSharePref.getInstance(mActivity).setCurrentCircle(currentCircleId);
                        userModel.setGroup_joining(currentCircleId);

                        UserSharePref.getInstance(mActivity).setUserInfo(userModel);
                        FirebaseDatabase.getInstance().getReference().
                                child("users").child(userModel.getUid()).child("group_joining").setValue(currentCircleId);

                        new MyGroupPresenter(new IMyGroupView() {
                            @Override
                            public void onGetMembersDataSucceed(List<TrackingModel> trackingModels, List<BatteryModel> batteryModels) {

                                for (TrackingModel trackingModel : trackingModels){

                                    if(! trackingModel.getUid().equals(FirebaseAuth.getInstance().
                                            getCurrentUser().getUid()))

                                        sendNotification(trackingModel.getUid(),UserSharePref.
                                                getInstance(mActivity).
                                                getUserInfo(mActivity).getFull_name());

                                    if(trackingModel.getUid().equals(trackingModels
                                                    .get(trackingModels.size() -1).getUid())){

                                        mActivity.finish();
                                        mActivity.startActivity(mActivity.getIntent());
                                    }
                                }
                            }

                            @Override
                            public void onGetMembersDataCancel(DatabaseError databaseError) {

                            }
                        }).onGetUserDataFromCurrentGroup(currentCircleId);
                    }
                });

            }
            return true;
        }
    }

    private void sendNotification(final String uId, final String userName) {

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                int SDK_INT = android.os.Build.VERSION.SDK_INT;
                if (SDK_INT > 8) {
                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                            .permitAll().build();
                    StrictMode.setThreadPolicy(policy);

                    try {
                        String jsonResponse;

                        URL url = new URL("https://onesignal.com/api/v1/notifications");
                        HttpURLConnection con = (HttpURLConnection) url.openConnection();
                        con.setUseCaches(false);
                        con.setDoOutput(true);
                        con.setDoInput(true);

                        con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                        con.setRequestProperty("Authorization", "Basic NTZiMjgwNjItMGYwMS00NWExLTgyYTgtODQ4YTkyZTg4ZDc4");
                        con.setRequestMethod("POST");

                        String strJsonBody = "{"
                                + "\"app_id\": \"8b12189d-1220-43fd-b3d1-cfa17ff7f65c\","
                                + "\"filters\": [{\"field\": \"tag\", \"key\": \"User_ID\", \"relation\": \"=\", \"value\": \"" + uId + "\"}],"
                                + "\"data\": {\"foo\": \"Join Circle\"},"
                                + "\"contents\": {\"en\": \"" + userName + " has joined your circle\"}"
                                + "}";


                        System.out.println("strJsonBody:\n" + strJsonBody);

                        byte[] sendBytes = strJsonBody.getBytes("UTF-8");
                        con.setFixedLengthStreamingMode(sendBytes.length);

                        OutputStream outputStream = con.getOutputStream();
                        outputStream.write(sendBytes);

                        int httpResponse = con.getResponseCode();
                        System.out.println("httpResponse: " + httpResponse);

                        if (httpResponse >= HttpURLConnection.HTTP_OK
                                && httpResponse < HttpURLConnection.HTTP_BAD_REQUEST) {
                            Scanner scanner = new Scanner(con.getInputStream(), "UTF-8");
                            jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
                            scanner.close();
                        } else {
                            Scanner scanner = new Scanner(con.getErrorStream(), "UTF-8");
                            jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
                            scanner.close();
                        }
                        System.out.println("jsonResponse:\n" + jsonResponse);

                    } catch (Throwable t) {
                        t.printStackTrace();
                    }
                }
            }
        });
    }
}
