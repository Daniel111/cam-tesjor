package kh.com.camtesjorapp.ui.resort.presenter;

/**
 * Created by mac on 5/25/2017 AD.
 */

public interface IResortPresenter {
    void retrieveData();
    void retrieveLoadMoreData();
    void retrievePopularData();
    void retrieveAllData();
    void retrieveProvinceData();
}
