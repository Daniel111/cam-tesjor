package kh.com.camtesjorapp.mvp.membercontrol.view;

import com.google.firebase.database.DatabaseError;

import kh.com.camtesjorapp.model.BatteryModel;
import kh.com.camtesjorapp.model.TrackingModel;

public interface IMemberControlView {

    void onChildChange(TrackingModel trackingModel, String s);

    void onChildRemove(TrackingModel trackingModel);

    void onChildEventCancel(DatabaseError databaseError);

    void onChildBatteryChange(BatteryModel batteryModel, String s);

    void onChildBatteryRemove(BatteryModel batteryModel);

    void onChildBatteryEventCancel(DatabaseError databaseError);
}
