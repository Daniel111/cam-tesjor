package kh.com.camtesjorapp.mvp.signup.view;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

/**
 * Created by mac on 4/13/2018 AD.
 */

public interface ISignUpView {

    void onCreateAccountSucceed(Task<AuthResult> task);
    void onCreateAccountFail();
}
