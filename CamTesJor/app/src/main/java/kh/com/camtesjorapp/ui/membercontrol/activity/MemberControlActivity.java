package kh.com.camtesjorapp.ui.membercontrol.activity;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.google.gson.Gson;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.base.BaseMapActivity;
import kh.com.camtesjorapp.model.BatteryModel;
import kh.com.camtesjorapp.model.TrackingModel;
import kh.com.camtesjorapp.model.UserTrackingInfo;
import kh.com.camtesjorapp.mvp.membercontrol.presenter.MemberControlPresenter;
import kh.com.camtesjorapp.mvp.membercontrol.view.IMemberControlView;
import kh.com.camtesjorapp.ui.membercontrol.adapter.UserInfoItemAdapter;

public class MemberControlActivity extends BaseMapActivity implements IMemberControlView {

    private LinkedHashMap<String, UserTrackingInfo> stringMarkerOptionsMap = new LinkedHashMap<>();
    private UserInfoItemAdapter userInfoItemAdapter;
    private List<BatteryModel> batteryModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        init();
        initBottomSheet();
    }

    @Override
    public boolean isDisplaySearchBar() {
        return false;
    }

    @Override
    protected boolean isDisplayTitle() {
        return false;
    }

    @Override
    public void onLocationCallBack(Object type, Location location) {
        super.onLocationCallBack(type, location);
        getmLocationRequestController().removeLocationUpdate();
        setmLocationRequestController(null);
        initMarkerOnMap();
    }


    @Override
    public void onChildChange(TrackingModel trackingModel, String s) {

        changeLocationMarker(trackingModel);

        if(FirebaseAuth.getInstance().getCurrentUser() == null) return;
        if(trackingModel.getUid().equals(FirebaseAuth.getInstance().getCurrentUser().getUid()) ){
            LatLng latLng = new LatLng(trackingModel.getLat(),trackingModel.getLng());
            getgMap().animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17), 1500, null);
        }
    }

    @Override
    public void onChildRemove(TrackingModel trackingModel) {
//        getgMap().clear();
//        getmClusterManager().clearItems();
//        trackingModels.remove(trackingModel);
//        initMarkerOnMap(trackingModels);
    }

    @Override
    public void onChildEventCancel(DatabaseError databaseError) {

    }

    @Override
    public void onChildBatteryChange(BatteryModel batteryModel, String s) {

        UserTrackingInfo userTrackingInfo = stringMarkerOptionsMap.get(s);
//
//        //if user tracking info = null mean new member is joining
//        if (userTrackingInfo == null){
//            userTrackingInfo = new
//                    UserTrackingInfo(getApplicationContext(), getmClusterManager(), trackingModel);
//            userTrackingInfo.setmIndexItSelf(stringMarkerOptionsMap.size()+1);
//
//        }

        batteryModels.set(userTrackingInfo.getmIndexItSelf(),batteryModel);
        userInfoItemAdapter.notifyItemChanged(userTrackingInfo.getmIndexItSelf());
    }

    @Override
    public void onChildBatteryRemove(BatteryModel batteryModel) {

    }

    @Override
    public void onChildBatteryEventCancel(DatabaseError databaseError) {

    }

    public void initBottomSheet() {
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(findViewById(R.id.root_layout));
        bottomSheetBehavior.setHideable(false);
    }

    @Override
    public void init() {
        super.init();
        batteryModels = Arrays.asList(new Gson().fromJson(getIntent().getStringExtra("battery"), BatteryModel[].class));
    }

    private void initMarkerOnMap() {

        List<TrackingModel> trackingModels = Arrays.asList(new Gson().fromJson(getIntent()
                .getStringExtra("tracking"), TrackingModel[].class));
        for (int i= 0 ; i <  trackingModels.size() ; i++){
            TrackingModel trackingModel = trackingModels.get(i);
            UserTrackingInfo userTrackingInfo = new UserTrackingInfo(getApplicationContext(), getmClusterManager(), trackingModel);
            userTrackingInfo.setmIndexItSelf(i);
            stringMarkerOptionsMap.put(trackingModel.getUid(), userTrackingInfo);
        }

        new MemberControlPresenter(this).onChildLocationEvent(getIntent().getStringExtra("id"));
        new MemberControlPresenter(this).onChildBatteryEvent(getIntent().getStringExtra("id"));

        setAllUsersLocation();
    }

    private void setAllUsersLocation() {
        RecyclerView allUserInfo = findViewById(R.id.recycler_view_user_loc);
        userInfoItemAdapter = new UserInfoItemAdapter(this, stringMarkerOptionsMap, batteryModels);
        allUserInfo.setLayoutManager(new LinearLayoutManager(this));
        allUserInfo.setAdapter(userInfoItemAdapter);
    }

    private void changeLocationMarker(TrackingModel trackingModel) {

        UserTrackingInfo userTrackingInfo = stringMarkerOptionsMap.get(trackingModel.getUid());

        //if user tracking info = null mean new member is joining
        if (userTrackingInfo == null){
            userTrackingInfo = new
                    UserTrackingInfo(getApplicationContext(), getmClusterManager(), trackingModel);
            userTrackingInfo.setmIndexItSelf(stringMarkerOptionsMap.size()+1);

        }

        //set new lat long to marker
        LatLng latLng = new LatLng(trackingModel.getLat(), trackingModel.getLng());
        userTrackingInfo.setLatLng(latLng);
        //handle move marker
        getMarkerMapRender().updateMarkerPos(userTrackingInfo, latLng, getgMap().getProjection());

        stringMarkerOptionsMap.put(trackingModel.getUid(),userTrackingInfo);
        userInfoItemAdapter.notifyItemChanged(userTrackingInfo.getmIndexItSelf());
    }

    public static void lunch(Context context) {
        Intent intent = new Intent(context, MemberControlActivity.class);
        context.startActivity(intent);
    }

    public static void lunch(Context context, String id, String tracking, String battery) {
        Intent intent = new Intent(context, MemberControlActivity.class);
        intent.putExtra("id", id);
        intent.putExtra("tracking", tracking);
        intent.putExtra("battery", battery);
        context.startActivity(intent);
    }

    public static void lunch(Context context, String id) {
        Intent intent = new Intent(context, MemberControlActivity.class);
        intent.putExtra("id", id);
    }
}
