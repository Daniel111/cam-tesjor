package kh.com.camtesjorapp.controller;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;

import kh.com.camtesjorapp.interfaces.LocationUpdaterInterface;

/**
 * @author Tony
 * @created on 6/22/2016
 */
public class LocationController
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        ResultCallback<LocationSettingsResult> {

    private static LocationController mInstance = null;
    private static final int REQUEST_LOCATION = 11119;
    private static final int REQUEST_CHECK_SETTINGS = 2;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    //added by ulseyha
    private static final float DISTANCE = 10;


    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private Boolean mRequestingLocationUpdates;
    private Context mContext;
    private Activity mActivity;

    private String mTag = "LocationController";
    private GoogleApiClient mGoogleApiClient;
    private LocationUpdaterInterface mLocationUpdaterInterface;
    private UserAllowPermission mUserAllowPermission;
    private boolean isGetLocation = false;
    private GetLocation mGetLocationListener;
    private boolean isPermissionGranted = false;

    private enum EnLocationType {
        CURRENT_LOCATION,
        REAL_TIME_LOCATION_CHANGE,
        LOCATION_NULL,
        PERMISSION_DENIED
    }

    public LocationController(Context context) {
        mContext = context;
        mActivity = (Activity) mContext;
        mRequestingLocationUpdates = false;
        isPermissionGranted = ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();
    }

    public synchronized static LocationController getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new LocationController(context);
        }
        return mInstance;
    }

    public static void clear() {
        mInstance = null;
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active ic_location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


        //added by ulseyha
        mLocationRequest.setSmallestDisplacement(DISTANCE);

    }

    private void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    /**
     * Check GPS if GPS is turn off show the dialog and ask for turn on.
     */
    public void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }

    /**
     * Check GPS turn on or turn off
     */
    public boolean checkGPS() {
        return ((LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE)).isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public boolean isGetLocation() {
        return isPermissionGranted && checkGPS();
    }

    /**
     * Check Permissions
     */
    public void checkPermissions() {
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_LOCATION);
            Log.d(mTag, "Show the dialog ask permission.");
        } else {
            checkLocationSettings();
            Log.d(mTag, "Permission Granted");
        }
    }

    public void checkPermissions(UserAllowPermission userAllowPermission) {
        mUserAllowPermission = userAllowPermission;
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_LOCATION);
        } else {
            checkLocationSettings();
            Log.d(mTag, "Permission Granted");
        }
    }

    public void removeUserAllowPermissionListener() {
        mUserAllowPermission = null;
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(mTag, "Connected to GoogleApiClient");

        //add by ulseyha
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && checkGPS()) {

        }

    }

    /**
     * Requests ic_location updates from the FusedLocationApi.
     */
    public void startLocationUpdates() {
        if (mGoogleApiClient.isConnected()) {
            if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && checkGPS()) {
                 LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this).setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        mRequestingLocationUpdates = true;
                        isGetLocation = false;
                        userPermissionLocation(true);
                        Log.d(mTag, "Start Location Updates.");
                    }
                });
            }

        } else
            Log.d(mTag, "StopLocationUpdates >>> not yet Connected to GoogleApiClient");
    }

    /**
     * Removes ic_location updates from the FusedLocationApi.
     */
    public void stopLocationUpdates() {
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent ic_location updates.
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this
            ).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(@NonNull Status status) {
                    mRequestingLocationUpdates = false;
                }
            });
        } else
            Log.d(mTag, "StopLocationUpdates >>> not yet Connected to GoogleApiClient");
    }

    private void userPermissionLocation(boolean type) {
        if (null != mLocationUpdaterInterface)
//            mLocationUpdaterInterface.onUserPermissionLocation(type);
        if (null != mUserAllowPermission)
            mUserAllowPermission.onUserAllowPermission(type);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (null != mLocationUpdaterInterface) {
//            mLocationUpdaterInterface.onRealTimeUpdateLocationCallBack(EnLocationType.REAL_TIME_LOCATION_CHANGE, location);
            if (!isGetLocation) {
                isGetLocation = true;
                mLocationUpdaterInterface.onLocationCallBack(EnLocationType.CURRENT_LOCATION, location);
            }
        }
        if (null != mGetLocationListener) mGetLocationListener.onGetLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(mTag, "Connection suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(mTag, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }

    /**
     * Connect to GoogleApiClient
     */
    public void onStart() {
        if (null != mGoogleApiClient)
            mGoogleApiClient.connect();
    }

    /**
     * Start Location Updates.
     */
    public void onResume() {
//        registerReceiverGPS();
        // ic_location updates if the user has requested them.
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            startLocationUpdates();
    }

    /**
     * Stop Location Updates.
     */
    public void onPause() {
//        unRegisterReceiverGPS();
        // Stop ic_location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            stopLocationUpdates();
    }

    /**
     * Disconnect from GoogleApiClient
     */
    public void onStop() {
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.d(mTag, "User turn on GPS.");
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.d(mTag, "Show the dialog to turn on GPS. ");
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(mActivity, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.d(mTag, "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.d(mTag, "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
        }
    }

    /**
     * This function call back when user granted or denied permission.
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    public void onRequestPermissionsResultCallBack(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        checkLocationSettings();
                        isPermissionGranted = true;
                        Log.d(mTag, "Permission Granted");
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        isPermissionGranted = false;
                        Log.d(mTag, "Permission Denied");
                        userPermissionLocation(false);
                    }
                }
            }
        }
    }

    /**
     * Call back when user press ok to turn on GPS or cancel to ignore.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    public void onActivityResultCallBack(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.d(mTag, "User turn on GPS.");
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.d(mTag, "User canceled turn on GPS.");
                        userPermissionLocation(false);
                        break;
                }
                break;
        }
    }

    /**
     * set Location Listener
     *
     * @param updateLocationListener
     */
    public void setLocationUpdateListener(LocationUpdaterInterface updateLocationListener) {
        mLocationUpdaterInterface = updateLocationListener;
    }

    public void removeLocationUpdateListener(LocationUpdaterInterface updateLocationListener) {
        mLocationUpdaterInterface = null;
    }

    public interface UserAllowPermission {
        void onUserAllowPermission(boolean type);
    }

    public interface GetLocation {
        void onGetLocation();
    }

    public void setLocationListener(GetLocation locationListener) {
        mGetLocationListener = locationListener;
    }

    public float getCurrentDistance(LatLng oldPosition, LatLng newPosition){

        float[] results = new float[1];
        Location.distanceBetween(oldPosition.latitude, oldPosition.longitude,
                newPosition.latitude, newPosition.longitude, results);

        return results[1];
    }
}