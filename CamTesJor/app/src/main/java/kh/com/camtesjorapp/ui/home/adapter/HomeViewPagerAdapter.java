package kh.com.camtesjorapp.ui.home.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;
import java.util.ArrayList;
import java.util.List;

import kh.com.camtesjorapp.ui.home.fragment.WallPaperFragment;
import kh.com.camtesjorapp.model.WallPaperModel;

public class HomeViewPagerAdapter extends FragmentPagerAdapter {

    private List<WallPaperModel> wallPaperModels = new ArrayList<>();

    public HomeViewPagerAdapter(Context context) {
        super(((AppCompatActivity) context).getSupportFragmentManager());
    }

    @Override
    public Fragment getItem(int position) {
        WallPaperFragment fra = WallPaperFragment.newInstance(getItemPosition(position).getBabFilePath());
        return fra;
    }

    @Override
    public int getCount() {
        if (wallPaperModels.size() == 0 || wallPaperModels.size() == 1) {
            return wallPaperModels.size();
        }
        return 2000;
    }


    private WallPaperModel getItemPosition(int position) {
        if (position > this.wallPaperModels.size() - 1)
            position = (position % this.wallPaperModels.size());
        return this.wallPaperModels.get(position);
    }

    public void addAll(List<WallPaperModel> wallPaperModels, Context context) {
        this.wallPaperModels.clear();
        this.wallPaperModels.addAll(wallPaperModels);
    }
}
