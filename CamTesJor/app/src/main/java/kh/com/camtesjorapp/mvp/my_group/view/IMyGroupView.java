package kh.com.camtesjorapp.mvp.my_group.view;

import com.google.firebase.database.DatabaseError;

import java.util.List;

import kh.com.camtesjorapp.model.BatteryModel;
import kh.com.camtesjorapp.model.TrackingModel;

public interface IMyGroupView {

    void onGetMembersDataSucceed(List<TrackingModel> trackingModel,List<BatteryModel> batteryModels);
    void onGetMembersDataCancel(DatabaseError databaseError);
}
