package kh.com.camtesjorapp.model;

/**
 * Created by mac on 3/13/2018 AD.
 */

public class TrackingModel {

    private String email;
    private String uid;
    private double lat;
    private double lng;
    private String full_name;
    private String image_url;

    public TrackingModel(){}

    public TrackingModel(String email, String uid, double lat, double lng, String full_name) {

        this.email = email;
        this.uid = uid;
        this.lat = lat;
        this.lng = lng;
        this.full_name = full_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}
