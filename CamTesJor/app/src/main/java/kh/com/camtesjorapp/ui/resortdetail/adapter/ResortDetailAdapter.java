package kh.com.camtesjorapp.ui.resortdetail.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import java.util.ArrayList;
import java.util.List;
import kh.com.camtesjorapp.ui.resortdetail.fragment.GuestHouseObjectFragment;
import kh.com.camtesjorapp.ui.resortdetail.fragment.HotelObjectFragment;
import kh.com.camtesjorapp.ui.resortdetail.fragment.ResortAreaObjectFragment;

public class ResortDetailAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> fragments;
    private String[] tabsName =new String[]{"Detail","Guest House","Hotel"};

    public ResortDetailAdapter(FragmentManager fm) {
        super(fm);
        fragments = new ArrayList<>();
        fragments.add(new ResortAreaObjectFragment());
//        fragments.add(new BusObjectFragment());
        fragments.add(new HotelObjectFragment());
        fragments.add(new GuestHouseObjectFragment());
    }

    @Override
    public Fragment getItem(int i) {
        return fragments.get(i);
    }

    @Override
    public int getCount() {
        return (fragments != null) ? fragments.size():0;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabsName[position];
    }
}
