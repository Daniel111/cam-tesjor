package kh.com.camtesjorapp.mvp.member.presenter;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.List;
import kh.com.camtesjorapp.model.TrackingModel;
import kh.com.camtesjorapp.mvp.member.view.IMemberView;

public class MemberPresenter implements IMemberPresenter {

    private IMemberView mMemberView;

    public MemberPresenter(IMemberView iMemberView) {
        this.mMemberView = iMemberView;
    }

    @Override
    public void onGetDataFromGroupAdmin(String groupId) {

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("group").child(groupId);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                List<TrackingModel> trackingModels = new ArrayList<>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    TrackingModel trackingModel = dataSnapshot1.getValue(TrackingModel.class);
                    trackingModels.add(trackingModel);
                }
                mMemberView.onGetMembersDataFromGroupAdminSucceed(trackingModels);
                databaseReference.removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mMemberView.onGetMembersDataCancel(databaseError);
                databaseReference.removeEventListener(this);
            }
        });
    }

    @Override
    public void onGetDataFromGroupJoining(String groupId) {

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("group").child(groupId);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                List<TrackingModel> trackingModels = new ArrayList<>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    TrackingModel trackingModel = dataSnapshot1.getValue(TrackingModel.class);
                    trackingModels.add(trackingModel);
                }
                mMemberView.onGetMembersDataFromGroupJoiningSucceed(trackingModels);
                databaseReference.removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mMemberView.onGetMembersDataCancel(databaseError);
                databaseReference.removeEventListener(this);
            }
        });

    }
}
