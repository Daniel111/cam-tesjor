package kh.com.camtesjorapp.constant;

/**
 * Created by mac on 6/8/2017 AD.
 */

public class Constants {

    //table names
    public static final String MAIN_SCREEN_PATH = "main_screen";
    public static final String PROVINCE_PATH = "province";
    public static final String RESORT_PATH = "resort";
    public static final String RESORT_DETAIL_PATH = "resort_detail";

    public static final String APP_VERSION = "app_version";

    //resort key, resort_detail key name Ex. angkor_wat
    public static final String RESORT_TYPE = "resort_type";
    public static final String RESORT_DETAIL_KEY = "resort_detail_key";

    public static final String RESORT_NAME = "resort_name";

    public static final String AD_MOB_APP_ID = "ca-app-pub-8380022434066903~8958719084";

    public static final int REQUEST_SIGN_IN_CODE = 123;
}
