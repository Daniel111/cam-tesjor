package kh.com.camtesjorapp.ui.creategroup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import java.util.UUID;
import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.base.BaseActivity;
import kh.com.camtesjorapp.model.TrackingModel;
import kh.com.camtesjorapp.model.UserModel;
import kh.com.camtesjorapp.ui.mygroup.MyGroupActivity;
import kh.com.camtesjorapp.sharepref.UserSharePref;

public class CreateGroup extends BaseActivity {

    private EditText groupName;
    private Button createGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);
        init();
    }

    @Override
    public boolean isDisplaySearchBar() {
        return false;
    }

    @Override
    protected String getToolbarTitle() {
        return "Create Group";
    }

    private void init() {
        groupName = findViewById(R.id.txt_groupName);
        createGroup = findViewById(R.id.create_group);
        createGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createGroup();
            }
        });
    }

    private void createGroup (){

        if(groupName.getText().toString().trim().isEmpty()){
            Toast.makeText(this,"Please input group name.",Toast.LENGTH_SHORT).show();
        }else {

            String groupID = generateString();
            UserModel userModel = UserSharePref.getInstance(this).getUserInfo(this);

            TrackingModel trackingModel = new TrackingModel();
            trackingModel.setFull_name(userModel.getFull_name());
            trackingModel.setImage_url(userModel.getImage_url());
            trackingModel.setUid(FirebaseAuth.getInstance().getCurrentUser().getUid());
            trackingModel.setLat(0);
            trackingModel.setLng(0);

            FirebaseDatabase.getInstance().getReference().child("users").
                    child(FirebaseAuth.getInstance().getCurrentUser().getUid()).
                    child("group_admin").setValue(groupID);

            FirebaseDatabase.getInstance().getReference().child("group").
                    child(groupID).
                    child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(trackingModel);

            Intent intent = new Intent();
            intent.putExtra(MyGroupActivity.GROUP_ID, groupID);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    public String generateString() {
        return UUID.randomUUID().toString();
    }

    public static void lunch(Activity context) {
        Intent intent = new Intent(context, CreateGroup.class);
        context.startActivityForResult(intent, MyGroupActivity.REQUEST_CREATE_GROUP);
    }

}
