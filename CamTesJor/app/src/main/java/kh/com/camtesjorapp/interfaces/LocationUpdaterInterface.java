package kh.com.camtesjorapp.interfaces;
import android.location.Location;

public interface LocationUpdaterInterface {

    void onLocationCallBack(Object type, Location location);
}
