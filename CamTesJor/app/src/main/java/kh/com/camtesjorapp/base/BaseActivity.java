package kh.com.camtesjorapp.base;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.onesignal.OSNotification;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.dialog.JoinCircleDialog;
import kh.com.camtesjorapp.share.CamTesjorApp;

@SuppressLint("Registered")
public class BaseActivity extends CamTesjorActivity {

    private FrameLayout mContentView;
    private ActionBar mActionBar;
    private TextView mButtonRetry;
    private LinearLayout mRootConnection;
    private FrameLayout mRootProgressbar;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_base);
        mContentView = findViewById(R.id.content_view);
        mRootConnection = findViewById(R.id.root_connection);
        mRootProgressbar = findViewById(R.id.root_progress);
        mButtonRetry = findViewById(R.id.retry);
        this.mButtonRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retryAction();
            }
        });
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        View view = getLayoutInflater().inflate(layoutResID, null, false);
        mContentView.addView(view);
        setupToolbar();
        setStatusBarColor(getStatusBarColor());
        initView();
    }

    @Override
    protected void onDestroy() {
        try {
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mRootProgressbar.getVisibility() == View.VISIBLE) {
            hideProgress();
            onBackClick();
        } else {
            super.onBackPressed();
        }
    }

    /*Setup Toolbar*/
    private void setupToolbar() {
        TextView titleBar = null;
        EditText searchBar = null;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setVisibility(isDisplayToolbar() ? View.VISIBLE : View.GONE);
            setSupportActionBar(toolbar);
            titleBar = (TextView) toolbar.findViewById(R.id.tv_titleBar);
            searchBar = (EditText) findViewById(R.id.edt_search);
        }
        try {
            mActionBar = getSupportActionBar();
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }

        if (mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(isDisplayHome());
            mActionBar.setDisplayShowTitleEnabled(false);
            titleBar.setVisibility(isDisplayTitle() ? View.VISIBLE : View.GONE);
            searchBar.setVisibility(isDisplaySearchBar() ? View.VISIBLE : View.GONE);
            if (getStringToolbarTitle() == null) {
                titleBar.setText(getToolbarTitle());
            } else {
                titleBar.setText(getStringToolbarTitle());
            }
        }
    }

    public boolean isDisplaySearchBar() {
        return true;
    }

    /*Set toolbar title*/
    protected String getToolbarTitle() {
        return "";
    }

    /*Set toolbar string title*/
    protected String getStringToolbarTitle() {
        return null;
    }

    /*Init component of view for activity*/
    protected void initView() {
    }

    /*Is display home button*/
    protected boolean isDisplayHome() {
        return true;
    }

    /*Is display title*/
    protected boolean isDisplayTitle() {
        return true;
    }

    /*Is display toolbar or not*/
    protected boolean isDisplayToolbar() {
        return true;
    }

    /*Action back pressed*/
    protected void onBackClick() {
        super.onBackPressed();
    }

    /*Show toast*/
    protected void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    /*Log debug*/
    protected void showLog(String key, String value) {
        Log.d(key, value);
    }

    /*Show progressbar*/
    public void showProgress() {
        this.mRootProgressbar.setVisibility(View.VISIBLE);
    }

    /*Hide progressbar*/
    public void hideProgress() {
        this.mRootProgressbar.setVisibility(View.GONE);
    }

    /*Show retry*/
    protected void showRetry() {
        this.mRootConnection.setVisibility(View.VISIBLE);
    }

    /*Retry action*/
    protected void retryAction() {
        this.mRootConnection.setVisibility(View.GONE);
    }

    /*Get progressbar*/
    protected FrameLayout getProgressbar() {
        return this.mRootProgressbar;
    }

    /*Get status bar color*/
    protected int getStatusBarColor() {
        return R.color.colorPrimary;
    }

    /*Set status bar color*/
    protected void setStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(BaseActivity.this, color));
        }
    }

}
