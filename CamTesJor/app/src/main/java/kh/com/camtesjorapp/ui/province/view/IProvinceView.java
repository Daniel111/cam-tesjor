package kh.com.camtesjorapp.ui.province.view;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

public interface IProvinceView {
    void onRetrieveDataSucceed(DataSnapshot snapshot);
    void onRetrieveDataError(DatabaseError databaseError);
}
