package kh.com.camtesjorapp.model;

/**
 * Created by mac on 5/26/2017 AD.
 */

public class HomeScreenModel {

    private String mProvinceName;
    private String mImageUrl;
    private String mResortKey;

    public String getmProvinceName() {
        return mProvinceName;
    }

    public void setmProvinceName(String mProvinceName) {
        this.mProvinceName = mProvinceName;
    }

    public String getmImageUrl() {
        return mImageUrl;
    }

    public void setmImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }

    public String getmResortKey() {
        return mResortKey;
    }

    public void setmResortKey(String mResortKey) {
        this.mResortKey = mResortKey;
    }

    public HomeScreenModel(String mProvinceName, String mImageUrl, String mProvinceKey) {
        this.mProvinceName = mProvinceName;
        this.mImageUrl = mImageUrl;
        this.mResortKey = mProvinceKey;
    }

    public HomeScreenModel(){};
}
