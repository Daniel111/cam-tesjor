package kh.com.camtesjorapp.fragment;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DatabaseError;
import com.google.maps.android.clustering.ClusterManager;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.controller.MarkerMapRender;
import kh.com.camtesjorapp.interfaces.LocationUpdaterInterface;
import kh.com.camtesjorapp.model.BatteryModel;
import kh.com.camtesjorapp.model.TrackingModel;
import kh.com.camtesjorapp.model.UserTrackingInfo;
import kh.com.camtesjorapp.mvp.membercontrol.presenter.MemberControlPresenter;
import kh.com.camtesjorapp.mvp.membercontrol.view.IMemberControlView;
import kh.com.camtesjorapp.mvp.my_group.presenter.MyGroupPresenter;
import kh.com.camtesjorapp.mvp.my_group.view.IMyGroupView;
import kh.com.camtesjorapp.service.BatteryService;
import kh.com.camtesjorapp.service.BootReceiver;
import kh.com.camtesjorapp.sharepref.UserSharePref;
import kh.com.camtesjorapp.ui.controlmember.ControlMemberActivity;
import kh.com.camtesjorapp.ui.membercontrol.adapter.UserInfoItemAdapter;
import kh.com.camtesjorapp.controller.LocationRequestController;

public class LocatorFragment extends Fragment implements
        OnMapReadyCallback,
        LocationUpdaterInterface, IMemberControlView, IMyGroupView {

    private ClusterManager<UserTrackingInfo> mClusterManager;
    private GoogleMap gMap;
    private LatLng latLng = null;
    private LocationRequestController mLocationRequestController;
    private MarkerMapRender markerMapRender;
    private Context mContext;
    private ControlMemberActivity mControlMemberActivity;
    private LinkedHashMap<String, UserTrackingInfo> stringMarkerOptionsMap = new LinkedHashMap<>();
    private List<BatteryModel> mBatteryModels = new ArrayList<>();
    private UserInfoItemAdapter userInfoItemAdapter;
    private String mGroupId;
    private View mView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mControlMemberActivity = (ControlMemberActivity) mContext;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.activity_map, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //init map
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mLocationRequestController.onPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.gMap = googleMap;

        mClusterManager = new ClusterManager<UserTrackingInfo>(mContext, gMap) {
            @Override
            public void onCameraIdle() {
                super.onCameraIdle();
            }
        };
        markerMapRender = new MarkerMapRender(mContext, gMap, mClusterManager);
        mClusterManager.setRenderer(markerMapRender);
        gMap.setOnCameraIdleListener(mClusterManager);
        gMap.setOnMarkerClickListener(mClusterManager);

        mLocationRequestController = new LocationRequestController(mContext,LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequestController.setLocationUpdateListener(this);
        mLocationRequestController.checkPermissions(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (latLng != null) {
            mLocationRequestController = new LocationRequestController(mContext, LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequestController.setLocationUpdateListener(this);
            mControlMemberActivity.showProgress();
        }
    }

    @Override
    public void onLocationCallBack(Object type, Location location) {

        if (location != null) {
            if (latLng == null) {
                latLng = new LatLng(location.getLatitude(), location.getLongitude());
            }
        } else {
            if (latLng == null) {
                latLng = new LatLng(0, 0);
            }
        }
        if (null != gMap && null != mClusterManager) {
            gMap.clear();
            mClusterManager.clearItems();
        }
        if(gMap != null ){
            mControlMemberActivity.hideProgress();
            gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15), 5, null);
            mLocationRequestController.removeLocationUpdate();
            mLocationRequestController = null;
            startServices();
            init(mView);
        }
    }

    @Override
    public void onChildChange(TrackingModel trackingModel, String s) {
        changeLocationMarker(trackingModel);
        mControlMemberActivity.hideProgress();
    }

    @Override
    public void onChildRemove(TrackingModel trackingModel) {

    }

    @Override
    public void onChildEventCancel(DatabaseError databaseError) {

    }

    @Override
    public void onChildBatteryChange(BatteryModel batteryModel, String s) {

        UserTrackingInfo userTrackingInfo = stringMarkerOptionsMap.get(s);

        if(userTrackingInfo == null){
            mBatteryModels.add(batteryModel);
            userInfoItemAdapter.notifyDataSetChanged();
            return;
        }
        mBatteryModels.set(userTrackingInfo.getmIndexItSelf(), batteryModel);
        userInfoItemAdapter.notifyItemChanged(userTrackingInfo.getmIndexItSelf());
    }

    @Override
    public void onChildBatteryRemove(BatteryModel batteryModel) {

    }

    @Override
    public void onChildBatteryEventCancel(DatabaseError databaseError) {

    }

    @Override
    public void onGetMembersDataSucceed(List<TrackingModel> trackingModels, List<BatteryModel> batteryModels) {

        if(trackingModels.size() == 0){
            mLocationRequestController = new LocationRequestController(mContext,LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequestController.setLocationUpdateListener(this);
            return;
        }

        for (int i = 0; i < trackingModels.size(); i++) {

            TrackingModel trackingModel = trackingModels.get(i);

            if(UserSharePref.getInstance(mContext).
                    getUserInfo(mContext).getUid().
                    equals(trackingModel.getUid())){

                trackingModel.setLat(latLng.latitude);
                trackingModel.setLng(latLng.longitude);
            }

            UserTrackingInfo userTrackingInfo = new UserTrackingInfo(mContext.getApplicationContext(), mClusterManager, trackingModel);
            userTrackingInfo.setmIndexItSelf(i);
            stringMarkerOptionsMap.put(trackingModel.getUid(), userTrackingInfo);
        }

        mBatteryModels.addAll(batteryModels);
        userInfoItemAdapter.notifyDataSetChanged();
        mControlMemberActivity.hideProgress();

        new MemberControlPresenter(this).onChildLocationEvent(mGroupId);
        new MemberControlPresenter(this).onChildBatteryEvent(mGroupId);
    }

    @Override
    public void onGetMembersDataCancel(DatabaseError databaseError) {

    }

    public void init(View view) {

        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(view.findViewById(R.id.root_layout));
        bottomSheetBehavior.setHideable(false);

        RecyclerView allUserInfo = view.findViewById(R.id.recycler_view_user_loc);
        userInfoItemAdapter = new UserInfoItemAdapter(mContext, stringMarkerOptionsMap, mBatteryModels);
        allUserInfo.setLayoutManager(new LinearLayoutManager(mContext));
        allUserInfo.setAdapter(userInfoItemAdapter);

        UserSharePref userSharePref = UserSharePref.getInstance(mContext);

        if(userSharePref.getCurrentCircle().isEmpty()){
            mGroupId = userSharePref.getUserInfo(mContext).getGroup_admin();
            userSharePref.setCurrentCircle(mGroupId);
        }else mGroupId = userSharePref.getCurrentCircle();

        new MyGroupPresenter(this).onGetUserDataFromCurrentGroup(mGroupId);
    }

    private void changeLocationMarker(TrackingModel trackingModel) {

        UserTrackingInfo userTrackingInfo = stringMarkerOptionsMap.get(trackingModel.getUid());

        //if user tracking info = null mean new member is joining
        if (userTrackingInfo == null) {
            userTrackingInfo = new
                    UserTrackingInfo(mContext.getApplicationContext(), mClusterManager, trackingModel);
            userTrackingInfo.setmIndexItSelf(stringMarkerOptionsMap.size() + 1);
        }

        //set new lat long to marker
        LatLng latLng = new LatLng(trackingModel.getLat(), trackingModel.getLng());
        userTrackingInfo.setLatLng(latLng);
        //handle move marker
        markerMapRender.updateMarkerPos(userTrackingInfo, latLng, gMap.getProjection());

        stringMarkerOptionsMap.put(trackingModel.getUid(), userTrackingInfo);
        userInfoItemAdapter.notifyItemChanged(userTrackingInfo.getmIndexItSelf());
    }

    private void startServices() {
        UserSharePref.getInstance(mContext).setLocation(latLng);
        Intent batteryIntent = new Intent(mContext, BatteryService.class);
        batteryIntent.putExtra(BootReceiver.ACTION_BOOT, BootReceiver.ACTION_BOOT);
        mContext.startService(batteryIntent);
    }
}
