package kh.com.camtesjorapp.ui.province.presenter;

import android.content.Context;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Timer;
import java.util.TimerTask;

import kh.com.camtesjorapp.constant.Constants;
import kh.com.camtesjorapp.ui.province.view.IProvinceView;
import kh.com.camtesjorapp.ui.resort.presenter.ResortPresenterCompl;
import kh.com.camtesjorapp.utils.NetworkUtil;


public class ProvincePresenterCompl implements IProvincePresenter, ValueEventListener {

    private IProvinceView mIProvinceView;
    private DatabaseReference mDatabase;
    private Context context;
    private boolean isGotResult;

    public ProvincePresenterCompl(IProvinceView iProvinceView){
        this.mIProvinceView = iProvinceView;
        this.context =(Context)iProvinceView;
    }

    @Override
    public void retrieveData() {
        onRequestTimeout();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        DatabaseReference value = mDatabase.child(Constants.PROVINCE_PATH);
        value.addListenerForSingleValueEvent(this);
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        mIProvinceView.onRetrieveDataSucceed(dataSnapshot);
        mDatabase.removeEventListener(this);
        isGotResult = true;
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        mIProvinceView.onRetrieveDataError(databaseError);
        mDatabase.removeEventListener(this);
        isGotResult = true;
    }

    private void onRequestTimeout(){

        boolean isHaveInternet = NetworkUtil.getConnectivityStatus(context) != NetworkUtil.TYPE_NOT_CONNECTED;

        if(!isHaveInternet) {
            final Timer timer = new Timer();
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    timer.cancel();
                    if (!isGotResult) {
                        mDatabase.removeEventListener(ProvincePresenterCompl.this);
                        mIProvinceView.onRetrieveDataError(null);
                    }
                }
            };
            timer.schedule(timerTask, 100);
        }
    }
}
