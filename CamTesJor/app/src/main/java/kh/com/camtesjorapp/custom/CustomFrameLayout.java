package kh.com.camtesjorapp.custom;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import java.util.List;
import java.util.TimerTask;
import io.realm.Realm;
import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.sharepref.UserSharePref;
import kh.com.camtesjorapp.ui.home.adapter.HomeViewPagerAdapter;
import kh.com.camtesjorapp.model.WallPaperModel;

public class CustomFrameLayout extends FrameLayout implements ViewPager.OnPageChangeListener {

    private CustomViewPager viewPager;
    private ViewPager.PageTransformer pageTransformer = new FadePageTransformer();
    private long switchingDuration = 10;
    private Handler handler;
    private HomeViewPagerAdapter adapter;
    private static CustomFrameLayout INSTANCE;

    public static CustomFrameLayout getInstance() {
        return INSTANCE;
    }

    public CustomFrameLayout(Context context) {
        super(context);
        viewInEditMode();
    }

    public CustomFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        viewInEditMode();
    }

    private void viewInEditMode() {
        if (!isInEditMode()) {
            build();
        }
    }

    public CustomFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        viewInEditMode();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        if (state == 1) {
            pause();
            setSwitchingDuration(5);
            if (viewPager.getBackground() != null)
                viewPager.setBackground(null);
        } else if (state == 0) {
            handler = null;
            start();
            setSwitchingDuration(10);
        }
        Log.d("TesjorAdvertisements", "state : " + state);
    }

    public long getOffsetDuration() {
        return 10000;
    }

    public long getSwitchingDuration() {
        return switchingDuration;
    }

    public void setSwitchingDuration(long switchingDuration) {
        this.switchingDuration = switchingDuration;
        viewPager.setScrollDurationFactor(getSwitchingDuration());
    }

    private void build() {

        INSTANCE = this;
        Realm.init(getContext());
        viewPager = new CustomViewPager(getContext());
        viewPager.setId(viewPager.hashCode());

        viewPager.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.cardview_dark_background));

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            viewPager.setBackground(getContext().getDrawable(R.drawable.angkor_thom_cambodi));
//        }

        viewPager.setScrollDurationFactor(getSwitchingDuration());
        adapter = new HomeViewPagerAdapter(getContext());
        viewPager.setAdapter(adapter);
        viewPager.setPageTransformer(false, getPageTransformer());
        viewPager.addOnPageChangeListener(this);
        LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = (Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
        addView(viewPager);
        reLoad();
    }

    public void reLoad() {

        List<WallPaperModel> result = UserSharePref.getInstance(getContext()).getBannerData(getContext());

        if (result != null) {
            pause();
            adapter.addAll(result,getContext());
            adapter.notifyDataSetChanged();
            int count = adapter.getCount();
            if (count > 1 && viewPager.getCurrentItem() < 100) {
                viewPager.setCurrentItem(count / 2);
            }
            start();
        }
    }

    public static void refresh() {
        CustomFrameLayout instance = CustomFrameLayout.getInstance();
        if (instance != null)
            instance.reLoad();
    }

    public void start() {
        if (handler == null) {
            handler = new Handler();
            handler.postDelayed(runnable, getOffsetDuration());
        }
    }

    public void pause() {
        if (handler != null) {
            handler.removeCallbacks(runnable);
            handler = null;
        }
    }

    public ViewPager.PageTransformer getPageTransformer() {
        return pageTransformer;
    }

    public class FadePageTransformer implements ViewPager.PageTransformer {
        public void transformPage(View view, float position) {
            view.setTranslationX(view.getWidth() * -position);
            if (position <= -1.0F || position >= 1.0F) {
                view.setAlpha(0.0F);
                view.setTranslationX(view.getWidth() * (-position + 1));
            } else if (position == 0.0F) {
                view.setAlpha(1.0F);
            } else {
                view.setAlpha(1.0F - Math.abs(position));
            }
        }
    }

    private Runnable runnable = new Runnable() {
        public void run() {
            try {
                ((Activity) getContext()).runOnUiThread(new TimerTask() {
                    @Override
                    public void run() {
                        viewPager.clearOnPageChangeListeners();
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
                        viewPager.addOnPageChangeListener(CustomFrameLayout.this);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
}
