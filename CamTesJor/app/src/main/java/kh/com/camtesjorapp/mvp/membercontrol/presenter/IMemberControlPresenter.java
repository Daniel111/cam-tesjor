package kh.com.camtesjorapp.mvp.membercontrol.presenter;

public interface IMemberControlPresenter {
    void onChildLocationEvent(String groupId);
    void onChildBatteryEvent(String groupId);
}
