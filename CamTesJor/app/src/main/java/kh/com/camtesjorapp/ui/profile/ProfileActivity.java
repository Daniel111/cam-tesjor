package kh.com.camtesjorapp.ui.profile;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.makeramen.roundedimageview.RoundedImageView;

import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.base.BaseActivity;
import kh.com.camtesjorapp.constant.Constants;
import kh.com.camtesjorapp.model.UserModel;
import kh.com.camtesjorapp.sharepref.UserSharePref;
import kh.com.camtesjorapp.ui.signin.SignInActivity;

public class ProfileActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuth.addAuthStateListener(new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() == null)
                    SignInActivity.lunch(ProfileActivity.this);
                else
                    init();
                firebaseAuth.removeAuthStateListener(this);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_SIGN_IN_CODE) {
            init();
        } else if (resultCode == RESULT_CANCELED && requestCode == Constants.REQUEST_SIGN_IN_CODE) {
            finish();
        }
    }

    @Override
    protected String getStringToolbarTitle() {
        return "Profile";
    }

    @Override
    public boolean isDisplaySearchBar() {
        return false;
    }

    private void init() {
        UserModel userModel = UserSharePref.getInstance(this).getUserInfo(this);

        if (userModel != null) {

            RoundedImageView roundedImageView = findViewById(R.id.image_view_profile_image);
            Glide.with(this).load(userModel.getImage_url()).error(R.drawable.com_facebook_profile_picture_blank_square).into(roundedImageView);
            ((TextView) findViewById(R.id.text_view_user_name)).setText(userModel.getFull_name());
            ((TextView) findViewById(R.id.text_view_user_email)).setText(userModel.getEmail() == null ? "No data." : userModel.getEmail());
        }
    }

    public static void lunch(Context context) {
        context.startActivity(new Intent(context, ProfileActivity.class));
    }
}
