package kh.com.camtesjorapp.model;

public class WallPaperModel {

    private String version;
    private String name;
    private String babFilePath;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBabFilePath() {
        return babFilePath;
    }

    public void setBabFilePath(String babFilePath) {
        this.babFilePath = babFilePath;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
