package kh.com.camtesjorapp.mvp.signup.presenter;

/**
 * Created by mac on 4/13/2018 AD.
 */

public interface ISignUpPresenter {
    void onCreateAccount(String email,String pass);
}
