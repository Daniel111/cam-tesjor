package kh.com.camtesjorapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import java.util.ArrayList;
import java.util.List;
import kh.com.camtesjorapp.fragment.LocatorFragment;
import kh.com.camtesjorapp.fragment.MemberFragment;
import kh.com.camtesjorapp.fragment.SettingFragment;

public class ControlMemberPagerAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> mFragmentList;
    private String[] strings = new String[]{"Locator","Member","Setting"};

    public ControlMemberPagerAdapter(FragmentManager fm) {
        super(fm);
        mFragmentList = new ArrayList<>();
        mFragmentList.add(new LocatorFragment());
        mFragmentList.add(new MemberFragment());
        mFragmentList.add(new SettingFragment());
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return strings[position];
    }
}
