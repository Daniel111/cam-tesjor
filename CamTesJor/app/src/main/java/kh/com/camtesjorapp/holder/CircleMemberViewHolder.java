package kh.com.camtesjorapp.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.dialog.AddMemberDialog;
import kh.com.camtesjorapp.dialog.JoinCircleDialog;

public class CircleMemberViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public CircleMemberViewHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (itemView.getId()){
            case R.id.linear_layout_join:
                new JoinCircleDialog(view.getContext()).show();
                break;
            default:
                new AddMemberDialog(view.getContext()).show();
        }
    }
}
