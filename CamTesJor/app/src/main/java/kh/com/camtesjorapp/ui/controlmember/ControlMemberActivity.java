package kh.com.camtesjorapp.ui.controlmember;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.onesignal.OSNotification;

import org.json.JSONException;
import org.json.JSONObject;

import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.adapter.ControlMemberPagerAdapter;
import kh.com.camtesjorapp.base.BaseActivity;
import kh.com.camtesjorapp.constant.Constants;
import kh.com.camtesjorapp.dialog.JoinCircleDialog;
import kh.com.camtesjorapp.model.UserModel;
import kh.com.camtesjorapp.mvp.my_group.presenter.MyGroupPresenter;
import kh.com.camtesjorapp.mvp.my_group.view.ICheckSignInView;
import kh.com.camtesjorapp.share.CamTesjorApp;
import kh.com.camtesjorapp.sharepref.UserSharePref;
import kh.com.camtesjorapp.ui.signin.SignInActivity;
import kh.com.camtesjorapp.utils.Utils;

public class ControlMemberActivity extends BaseActivity implements ICheckSignInView {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control_member2);
        new MyGroupPresenter(this).onCheckUserSignIn();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_SIGN_IN_CODE) init();
        else if (resultCode == RESULT_CANCELED && requestCode == Constants.REQUEST_SIGN_IN_CODE) finish();
    }

    @Override
    public void onUserAlreadySignIn() {
        init();
    }

    @Override
    public void onUserNotYetSignIn() {
        SignInActivity.lunch(ControlMemberActivity.this);
    }

    @Override
    public boolean isDisplaySearchBar() {
        return false;
    }

    @Override
    protected String getToolbarTitle() {
        return getResources().getString(R.string.control_member);
    }

    @Override
    public void onDialogClick() {
        finish();
        startActivity(getIntent());
    }

    private void init() {

        showProgress();
        createGroupAdmin();
        ViewPager viewPager = findViewById(R.id.view_pager_member_control);
        ControlMemberPagerAdapter controlMemberPagerAdapter = new ControlMemberPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(controlMemberPagerAdapter);
        viewPager.setOffscreenPageLimit(3);
        TabLayout tabLayout = findViewById(R.id.tab_layout_member_control);
        tabLayout.setupWithViewPager(viewPager);

        TextView locatorTab = (TextView) LayoutInflater.from(this).inflate(R.layout.tab_layout_item, null);
        locatorTab.setText("Locator");
        locatorTab.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_placeholder, 0, 0);

        tabLayout.getTabAt(0).setCustomView(locatorTab);

        TextView memberTab = (TextView) LayoutInflater.from(this).inflate(R.layout.tab_layout_item, null);
        memberTab.setText("Member");
        memberTab.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_people, 0, 0);

        tabLayout.getTabAt(1).setCustomView(memberTab);

        TextView settingsTab = (TextView) LayoutInflater.from(this).inflate(R.layout.tab_layout_item, null);
        settingsTab.setText("Settings");
        settingsTab.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_settings, 0, 0);

        tabLayout.getTabAt(2).setCustomView(settingsTab);
    }

    private void createGroupAdmin(){

        UserModel userModel = UserSharePref.getInstance(this).getUserInfo(this);

        if(userModel.getGroup_admin() == null){

            String groupID = Utils.getRandomString(6);
            userModel.setGroup_admin(groupID);
            UserSharePref.getInstance(this).setCurrentCircle(groupID);
            UserSharePref.getInstance(this).setUserInfo(userModel);

            FirebaseDatabase.getInstance().getReference().child("users").
                    child(FirebaseAuth.getInstance().getCurrentUser().getUid()).
                    child("group_admin").setValue(groupID);
        }
    }

    public static void lunch(Context context) {
        context.startActivity(new Intent(context, ControlMemberActivity.class));
    }

}
