package kh.com.camtesjorapp.sharepref;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.preference.PreferenceManager;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import kh.com.camtesjorapp.model.UserModel;
import kh.com.camtesjorapp.model.WallPaperModel;

public class UserSharePref {

    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    private static UserSharePref preferenceUtil;
    private static final String API_USER_MODEL = "user_model";
    private static final String API_BANNER = "banner";
    private static final String API_LOCATION = "location";
    private static final String API_CURRENT_CIRCLE = "current_circle";

    public static UserSharePref getInstance(Context context) {
        if (preferenceUtil == null) {
            preferenceUtil = new UserSharePref(context);
        }
        return preferenceUtil;
    }

    private UserSharePref(Context context) {
        sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPref.edit();
    }

    public void setUserInfo(UserModel userModel){
        String jsonUser = new Gson().toJson(userModel);
        editor.putString(API_USER_MODEL, jsonUser).commit();
    }

    public UserModel getUserInfo(Context context){
        sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        String json = sharedPref.getString(API_USER_MODEL, "");
        return new Gson().fromJson(json,UserModel.class);
    }

    public void storeBannerData(List<WallPaperModel> userModel) {
        String jsonUser = new Gson().toJson(userModel);
        editor.putString(API_BANNER, jsonUser).commit();
    }

    public void resetUserModel() {
        editor.putString(API_USER_MODEL, null);
        editor.putString(API_CURRENT_CIRCLE, null);
        editor.commit();
    }

    public List<WallPaperModel> getBannerData(Context context) {
        sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        String json = sharedPref.getString(API_BANNER, "");

        try {
            return Arrays.asList(new Gson().fromJson(json,WallPaperModel[].class));
        }catch (NullPointerException e){
            return new ArrayList<>();
        }
    }

    public void setLocation(LatLng location) {

        String userLocation= new Gson().toJson(location);
        editor.putString(API_LOCATION, userLocation).commit();
    }

    public void setCurrentCircle(String currentCircle){
        editor.putString(API_CURRENT_CIRCLE,currentCircle);
        editor.commit();
    }

    public String getCurrentCircle(){
        return sharedPref.getString(API_CURRENT_CIRCLE,"");
    }

    public LatLng getOldLocation(Context context) {
        sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        String json = sharedPref.getString(API_LOCATION, "");
        return new Gson().fromJson(json,LatLng.class);
    }
}

