package kh.com.camtesjorapp.model;

/**
 * Created by mac on 5/25/2017 AD.
 */

public class ResortModel {

    public String description;
    public String name;
    public String image_url;
    public String resort_detail_name;

    public String getResortDetailName() {
        return resort_detail_name;
    }

    public ResortModel(){}

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public String getImage_url() {
        return image_url;
    }
}
