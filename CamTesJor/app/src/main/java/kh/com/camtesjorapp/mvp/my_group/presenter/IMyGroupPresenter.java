package kh.com.camtesjorapp.mvp.my_group.presenter;

public interface IMyGroupPresenter {

    void onCheckUserSignIn();
    void onGetUserDataFromCurrentGroup(String groupId);
}
