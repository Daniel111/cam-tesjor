package kh.com.camtesjorapp.mvp.my_group.view;

/**
 * Created by mac on 5/18/2018 AD.
 */

public interface ICheckSignInView {

    void onUserAlreadySignIn();
    void onUserNotYetSignIn();
}
