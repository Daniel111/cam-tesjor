package kh.com.camtesjorapp.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DatabaseError;

import java.util.ArrayList;
import java.util.List;

import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.adapter.MemberAdapter;
import kh.com.camtesjorapp.model.TrackingModel;
import kh.com.camtesjorapp.model.UserModel;
import kh.com.camtesjorapp.mvp.member.presenter.MemberPresenter;
import kh.com.camtesjorapp.mvp.member.view.IMemberView;
import kh.com.camtesjorapp.sharepref.UserSharePref;
import kh.com.camtesjorapp.ui.controlmember.ControlMemberActivity;

public class MemberFragment extends Fragment implements IMemberView{

    private Context mContext;
    private MemberAdapter mMemberOfGroupAdminAdapter;
    private MemberAdapter mMemberOfGroupJoiningAdapter;
    private List<TrackingModel> mAdminTrackingModels = new ArrayList<>();
    private List<TrackingModel> mJoiningTrackingModels = new ArrayList<>();
    private ControlMemberActivity mControlMemberActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mControlMemberActivity = (ControlMemberActivity)context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_member, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    @Override
    public void onGetMembersDataFromGroupJoiningSucceed(List<TrackingModel> trackingModel) {

        mJoiningTrackingModels.addAll(trackingModel);
        mMemberOfGroupJoiningAdapter.notifyDataSetChanged();
        mControlMemberActivity.hideProgress();

    }

    @Override
    public void onGetMembersDataFromGroupAdminSucceed(List<TrackingModel> trackingModel) {

        mAdminTrackingModels.addAll(trackingModel);
        mMemberOfGroupAdminAdapter.notifyDataSetChanged();
        mControlMemberActivity.hideProgress();
    }

    @Override
    public void onGetMembersDataCancel(DatabaseError databaseError) {
        mControlMemberActivity.hideProgress();
    }

    private void init(View view){

        mControlMemberActivity.showProgress();

        //group admin's recycler view
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view_create_circle);
        recyclerView.setLayoutManager(new GridLayoutManager(mContext,4,GridLayoutManager.VERTICAL,false));
        mMemberOfGroupAdminAdapter = new MemberAdapter(mContext,mAdminTrackingModels,R.layout.add_member_layout);
        recyclerView.setAdapter(mMemberOfGroupAdminAdapter);

        //group joining's recycler view
        RecyclerView joinCircle = view.findViewById(R.id.recycler_view_join_circle);
        joinCircle.setLayoutManager(new GridLayoutManager(mContext,4,GridLayoutManager.VERTICAL,false));
        mMemberOfGroupJoiningAdapter = new MemberAdapter(mContext,mJoiningTrackingModels,R.layout.join_member_layout);
        joinCircle.setAdapter(mMemberOfGroupJoiningAdapter);

        UserModel userModel = UserSharePref.getInstance(mContext).getUserInfo(mContext);
        new MemberPresenter(this).onGetDataFromGroupAdmin(userModel.getGroup_admin());

        if(userModel.getGroup_joining() != null)
            new MemberPresenter(this).onGetDataFromGroupJoining(userModel.getGroup_joining());
    }
}
