package kh.com.camtesjorapp.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import kh.com.camtesjorapp.model.BatteryModel;
import kh.com.camtesjorapp.model.UserModel;
import kh.com.camtesjorapp.sharepref.UserSharePref;

public class BatteryService extends Service {

    public static final String BATTERY_UPDATE = "battery";
    public static final String HANDLE_REBOOT = "HANDLE_REBOOT";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        if (intent != null && intent.hasExtra(BootReceiver.ACTION_BOOT)){
            AlarmReceiver.startAlarms(this.getApplicationContext());
        }
        if (intent != null && intent.hasExtra(BATTERY_UPDATE)){
            new BatteryCheckAsync().execute();
        }
        return START_STICKY;
    }

    @SuppressLint("StaticFieldLeak")
    private class BatteryCheckAsync extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... arg0) {
            //Battery State check - create log entries of current battery state
            IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            Intent batteryStatus = BatteryService.this.registerReceiver(null, ifilter);

            int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                    status == BatteryManager.BATTERY_STATUS_FULL;

            int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

            UserModel userModel = UserSharePref.getInstance(BatteryService.this).getUserInfo(BatteryService.this);

            if(userModel != null){

                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().
                        child("group_battery").child(UserSharePref.getInstance(BatteryService.this).getCurrentCircle()).
                        child(userModel.getUid());

                Long tsLong = System.currentTimeMillis();

                BatteryModel batteryModel = new BatteryModel();
                batteryModel.setCharging(isCharging);
                batteryModel.setLevel(level);
                batteryModel.setScale(scale);
                batteryModel.setTime_stamp(tsLong);

                databaseReference.setValue(batteryModel);

            }
            return true;
        }
    }
}
