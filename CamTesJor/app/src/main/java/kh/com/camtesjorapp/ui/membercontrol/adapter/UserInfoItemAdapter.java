package kh.com.camtesjorapp.ui.membercontrol.adapter;

import android.content.Context;
import android.media.Image;
import android.os.AsyncTask;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.model.BatteryModel;
import kh.com.camtesjorapp.model.UserTrackingInfo;
import kh.com.camtesjorapp.sharepref.UserSharePref;
import kh.com.camtesjorapp.utils.Utils;

public class UserInfoItemAdapter extends RecyclerView.Adapter<UserInfoItemAdapter.ViewHolder> {

    private LinkedHashMap<String, UserTrackingInfo> mTrackingList;
    private List<BatteryModel> batteryModels;
    private Context mContext;

    public UserInfoItemAdapter(Context context, LinkedHashMap<String, UserTrackingInfo> mTrackingList, List<BatteryModel> batteryModels) {
        this.mContext = context;
        this.mTrackingList = mTrackingList;
        this.batteryModels = batteryModels;
    }

    @Override
    public UserInfoItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.user_tracking_item, parent, false);
        return new UserInfoItemAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(UserInfoItemAdapter.ViewHolder holder, int position) {
        UserTrackingInfo userTrackingInfo = (new ArrayList<>(mTrackingList.values()).get(position));
        Glide.with(mContext).load(userTrackingInfo.getmImageUrl()).into(holder.userImage);

        holder.userName.setText(
                userTrackingInfo.getTrackingModel().getUid().
                        equals(UserSharePref.getInstance(mContext).getUserInfo(mContext).getUid())
                        ? userTrackingInfo.getmName() +" (You)": userTrackingInfo.getmName());

        holder.mDateUpdated.setText(Utils.getFormattedDate(mContext,batteryModels.get(position).getTime_stamp()));
        holder.mFlash.setVisibility(batteryModels.get(position).isCharging()?View.VISIBLE:View.GONE);
        setBatterLevel(holder.txtBatteryLevel,holder.mBattery,position);
        new GetAddressAsyncTask(holder.mAddress,userTrackingInfo.getLatLng().latitude,userTrackingInfo.getLatLng().longitude).execute();
    }

    @Override
    public int getItemCount() {
        return (mTrackingList != null) ? mTrackingList.size() : 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private RoundedImageView userImage;
        private TextView userName;
        private TextView txtBatteryLevel;
        private TextView mAddress;
        private TextView mDateUpdated;
        private ImageView mBattery;
        private ImageView mFlash;

        public ViewHolder(View itemView) {
            super(itemView);

            userImage = itemView.findViewById(R.id.user_img);
            userName = itemView.findViewById(R.id.txt_user_name);
            txtBatteryLevel = itemView.findViewById(R.id.txt_battery);
            mAddress = itemView.findViewById(R.id.text_view_address);
            mBattery = itemView.findViewById(R.id.image_view_battery);
            mFlash = itemView.findViewById(R.id.image_view_flash);
            mDateUpdated = itemView.findViewById(R.id.text_view_date);
        }
    }

    class GetAddressAsyncTask extends AsyncTask<String,String,String>{

        private TextView mAddress;
        private double mLat;
        private double mLong;

        public GetAddressAsyncTask(TextView address, double lat, double longitude){
            this.mAddress = address;
            this.mLat = lat;
            this.mLong = longitude;
        }

        @Override
        protected String doInBackground(String... strings) {
            return Utils.getCompleteAddressString(mContext, mLat, mLong);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mAddress.setText(s);
        }
    }

    private void setBatterLevel(TextView battery, ImageView imageView, int pos){

        int level = (int) batteryModels.get(pos).getLevel();
        String levelText = level+"%";

       if(level >= 90){
            battery.setText(levelText);
           imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_battery_full));
        }
        else if(level >= 50){
            battery.setText(levelText);
            imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_battery_75));
        }
        else if(level >= 25){
            battery.setText(levelText);
            imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_battery_50));
        }
        else{
            battery.setText(levelText);
            imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_battery_25));
        }

    }
}
