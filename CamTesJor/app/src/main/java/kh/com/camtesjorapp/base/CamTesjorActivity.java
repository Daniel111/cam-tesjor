package kh.com.camtesjorapp.base;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.firebase.client.Firebase;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.constant.IntentKeys;
import kh.com.camtesjorapp.model.BatteryModel;
import kh.com.camtesjorapp.model.UserModel;
import kh.com.camtesjorapp.service.BatteryService;
import kh.com.camtesjorapp.sharepref.UserSharePref;
import kh.com.camtesjorapp.utils.NetworkUtil;

@SuppressLint("Registered")
public class CamTesjorActivity extends AppCompatActivity {

    private ServiceBroadcastReceiver receiver = new ServiceBroadcastReceiver();
    private Snackbar snackbar;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        registerReceiver();
        Log.i("ActivityLifecycle", String.format("onCreate (%s)", this.getClass().getSimpleName()));
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        registerReceiver();
        Log.d("ActivityLifecycle", String.format("onRestart (%s)", this.getClass().getSimpleName()));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unRegisterReceiver();
        Log.d("ActivityLifecycle", String.format("onPause (%s)", this.getClass().getSimpleName()));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        registerReceiver();
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("ActivityLifecycle", String.format("onActivityResult (%s)", this.getClass().getSimpleName()));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unRegisterReceiver();
        Log.d("ActivityLifecycle", String.format("onStop (%s)", this.getClass().getSimpleName()));
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.d("ActivityLifecycle", String.format("onPostResume (%s)", this.getClass().getSimpleName()));
//        registerReceiver();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("ActivityLifecycle", String.format("onResume (%s)", this.getClass().getSimpleName()));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unRegisterReceiver();
        Log.d("ActivityLifecycle", String.format("onDestroy (%s)", this.getClass().getSimpleName()));
    }


    public IntentFilter onSetIntentFilter(IntentFilter filter) {
        return filter;
    }

    public void onRegisterReceiver(Context context) {
        Log.d("SocketReceiver", "Register with " + context.getClass().getSimpleName());
    }

    public void onUnRegisterReceiver(Context context) {
        Log.d("SocketReceiver", "Unregister with " + context.getClass().getSimpleName());
    }

    public void onConnectionReceiveBroadcast(boolean isHaveInternet, int statusType) {
        onConnectionChange(isHaveInternet, statusType);
    }

    public class ServiceBroadcastReceiver extends BroadcastReceiver {

        private IntentFilter filter;

        public ServiceBroadcastReceiver() {
            filter = new IntentFilter();
            filter = onSetIntentFilter(filter);
            filter.addAction(IntentKeys.EVENT_EMIT_RESULT);
            filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            filter.addAction(Intent.ACTION_BATTERY_CHANGED);

        }

        public void registerReceiver(Context context) {
            try {
                Log.i(this.getClass().getSimpleName(), "Call Register with " + context.getClass().getSimpleName());
                context.registerReceiver(this, filter);
                onRegisterReceiver(context);
            } catch (Exception e) {
                Log.w(this.getClass().getSimpleName(), "Cannot register with " + context.getClass().getSimpleName() + " : " + e.getMessage());
            }
        }

        public void unRegisterReceiver(Context context) {
            try {
                Log.i(this.getClass().getSimpleName(), "Call Unregister with " + context.getClass().getSimpleName());
                context.unregisterReceiver(this);
                onUnRegisterReceiver(context);
            } catch (Exception e) {
                Log.w(this.getClass().getSimpleName(), "Cannot unregister with " + context.getClass().getSimpleName() + " : " + e.getMessage());
            }
        }

        @Override
        public void onReceive(Context context, Intent intent) {

            if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())) {

                String status = NetworkUtil.getConnectivityStatusString(context);
                int statusId = NetworkUtil.getConnectivityStatus(context);
                boolean isHaveInternet = statusId != NetworkUtil.TYPE_NOT_CONNECTED;
                onConnectionReceiveBroadcast(isHaveInternet, statusId);

            } else if (Intent.ACTION_BATTERY_CHANGED.equals(intent.getAction())) {
                sentBatteryStatus(intent);
            } else {
                String data = intent.getStringExtra(IntentKeys.EXTRA_DATA);
                onNotificationReceiveBroadcast(data);
            }
        }
    }

    private void sentBatteryStatus(Intent intent) {

        UserModel userModel = UserSharePref.getInstance(this).getUserInfo(this);

        if (userModel != null) {

            int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                    status == BatteryManager.BATTERY_STATUS_FULL;

            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().
                    child("group_battery").child(UserSharePref.getInstance(this).getCurrentCircle()).
                    child(userModel.getUid());

            Long tsLong = System.currentTimeMillis();

            BatteryModel batteryModel = new BatteryModel();
            batteryModel.setCharging(isCharging);
            batteryModel.setLevel(level);
            batteryModel.setScale(scale);
            batteryModel.setTime_stamp(tsLong);

            databaseReference.setValue(batteryModel);
        }
    }

    public void onNotificationReceiveBroadcast(String data) {

        try {
            JSONObject jsonObject = new JSONObject(data);

            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle("Join Circle");
            dialog.setMessage(jsonObject.optString("body"));
            dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();

                }
            });
            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    onDialogClick();
                }
            });
            dialog.create().show();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void onDialogClick() {

    }

    public void registerReceiver() {
        receiver.registerReceiver(this);
    }

    public void unRegisterReceiver() {
        receiver.unRegisterReceiver(this);
    }

    public void onConnectionChange(boolean isHaveInternet, int socketStatus) {

        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);
        if (coordinatorLayout != null) {

            if (snackbar != null) snackbar.dismiss();
            snackbar = Snackbar.make(coordinatorLayout, "No internet connection.", Snackbar.LENGTH_INDEFINITE);
            snackbar.setAction("Dismiss", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snackbar.dismiss();
                }
            });
            if (isHaveInternet) snackbar.dismiss();
            else snackbar.show();
        }

        refresh();
    }

    public void refresh() {
    }
}
