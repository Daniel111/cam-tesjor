package kh.com.camtesjorapp.ui.home.presenter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import kh.com.camtesjorapp.ui.home.view.IHomeView;

public class HomePresenterCompl implements IHomePresenter {

    private IHomeView mIHomeView;
    private String mRequestType;

    public HomePresenterCompl(IHomeView iHomeView,String requestType){
        this.mIHomeView = iHomeView;
        this.mRequestType = requestType;
    }

    @Override
    public void retrieveData() {

        DatabaseReference main_screen = FirebaseDatabase.
                getInstance().
                getReference().
                child(mRequestType);

        main_screen.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mIHomeView.onRetrieveDataSucceed(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mIHomeView.onRetrieveDataError(databaseError);
            }
        });
    }
}
