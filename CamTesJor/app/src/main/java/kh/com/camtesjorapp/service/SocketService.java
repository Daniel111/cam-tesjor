package kh.com.camtesjorapp.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import kh.com.camtesjorapp.BuildConfig;
import kh.com.camtesjorapp.constant.IntentKeys;
import kh.com.camtesjorapp.utils.NetworkUtil;

public class SocketService extends Service {

    private final IBinder binder = new ServiceBinder();
    private final String TAG = "SocketService";
    private int connectionState = 0;
    private boolean isHaveInternet = true;

    public Service onGetService() {
        return this;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    private NetworkChangeReceiver networkReceiver = new NetworkChangeReceiver();

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "onCreate (" + this.getClass().getSimpleName() + ")");
        networkReceiver.registerReceiver(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStartCommand​​​​​​ " + startId);
        super.onStartCommand(intent, flags, startId);
        networkReceiver.registerReceiver(this);
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "Service Destroyed");
        networkReceiver.unRegisterReceiver(this);
        super.onDestroy();
    }


    public void sendBroadcast(Intent intent) {
        if (intent != null) {
            if (BuildConfig.DEBUG)
                Log.d(TAG, "sendBroadcast :>>>" + intent.getAction());
//            super.sendBroadcast(intent, IntentKeys.PERMISSION_ADMIN);
            super.sendBroadcast(intent);
            intent = null;
        }
    }

    private void sendBroadcastConnectionState() {
        Intent intent = new Intent(IntentKeys.EVENT_ON_CONNECTION_CHANGE);
        intent.putExtra(IntentKeys.EXTRA_CONNECTION_STATE, isHaveInternet);
        intent.putExtra(IntentKeys.EXTRA_SOCKET_CONNECTION_STATE, this.connectionState);
        sendBroadcast(intent);
    }

    public class ServiceBinder extends Binder {
        Service getService() {
            return onGetService();
        }
    }

    private class NetworkChangeReceiver extends BroadcastReceiver {

        private boolean isNetworkConnected() {
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnectedOrConnecting();
        }

        public void registerReceiver(Context context) {
            Log.i(this.getClass().getSimpleName(), "Call Register with " + context.getClass().getSimpleName());
            try {
                context.registerReceiver(this, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            } catch (Exception e) {
                Log.w(this.getClass().getSimpleName(), "Cannot register with " + context.getClass().getSimpleName() + " : " + e.getMessage());
            }
        }

        public void unRegisterReceiver(Context context) {
            Log.i(this.getClass().getSimpleName(), "Call Unregister with " + context.getClass().getSimpleName());
            try {
                context.unregisterReceiver(this);
            } catch (Exception e) {
                Log.w(this.getClass().getSimpleName(), "Cannot unregister with " + context.getClass().getSimpleName() + " : " + e.getMessage());
            }
        }

        @Override
        public void onReceive(final Context context, final Intent intent) {

            String status = NetworkUtil.getConnectivityStatusString(context);
            int statusId = NetworkUtil.getConnectivityStatus(context);
            if (BuildConfig.DEBUG)
                Log.d("NetworkChangeReceiver", "status : " + status + " : " + statusId + " : " + connectionState);
            isHaveInternet = statusId != NetworkUtil.TYPE_NOT_CONNECTED;
            sendBroadcastConnectionState();
        }
    }
}
