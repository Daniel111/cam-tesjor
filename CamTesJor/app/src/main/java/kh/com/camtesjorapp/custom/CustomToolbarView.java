package kh.com.camtesjorapp.custom;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.annotation.StyleRes;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.balysv.materialmenu.MaterialMenuView;

import java.util.Locale;

import kh.com.camtesjorapp.R;

public class CustomToolbarView extends FrameLayout implements View.OnClickListener {

    private FrameLayout backGround;
    private ScaleAnimation scaleAnimation;
    private ObjectAnimator searchAnim;
    private ImageView iconSearch;
    private ImageView search;
    private RelativeLayout iconNoti;
    private RelativeLayout iconOrder;
    private RelativeLayout iconChat;
    private TextView textView;
    private static float x;
    private static float bgSearchX;
    private Point size;
    private ObjectAnimator objectAnimator;
    private Boolean isLogin;
    private TextView notiBadge, chatBadge, orderBadge;
    private boolean isJump, isPending, isDoneAnimate;
    private ImageView mImageViewFilling;

    public TextView getTextView() {
        return textView;
    }

    public CustomToolbarView(@NonNull Context context) {
        super(context);
        init();
    }

    public CustomToolbarView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomToolbarView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CustomToolbarView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr, @StyleRes int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.custom_toolbar, this);
        if (isInEditMode()) {
            return;
        }
        findViewById(R.id.material_menu_view).setOnClickListener(this);
        mImageViewFilling = (ImageView) findViewById(R.id.imageview_animation_list_filling);
        mImageViewFilling.setVisibility(INVISIBLE);
        mImageViewFilling.setBackground(null);

        iconSearch = (ImageView) findViewById(R.id.ic_search);
        search = (ImageView) findViewById(R.id.search);
        iconNoti = (RelativeLayout) findViewById(R.id.layout_noti);
        iconOrder = (RelativeLayout) findViewById(R.id.layout_order);
        iconChat = (RelativeLayout) findViewById(R.id.layout_chat);

        notiBadge = (TextView) findViewById(R.id.noti_badge);
        chatBadge = (TextView) findViewById(R.id.chat_badge);
        orderBadge = (TextView) findViewById(R.id.order_badge);

        iconNoti.setOnClickListener(this);
        iconOrder.setOnClickListener(this);
        iconChat.setOnClickListener(this);

        backGround = (FrameLayout) findViewById(R.id.background);
        backGround.setOnClickListener(this);
        bgSearchX = backGround.getX();
        x = iconSearch.getX() + iconSearch.getWidth();
        textView = (TextView) findViewById(R.id.search_box);
        Display display = ((Activity) getContext()).getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
    }

    public TextView getTextViewSearch() {
        return textView;
    }

    public void setMode(boolean isLogin) {
        if (this.isLogin == null || isLogin != this.isLogin) {
            if (isLogin)
                startAnimationAfterLogin();
            else
                startAnimationBeforeLogin();
        }
        this.isLogin = isLogin;
    }

    public void startAnimationAfterLogin() {
        iconSearch.setVisibility(VISIBLE);
        backGround.clearAnimation();
        search.setVisibility(INVISIBLE);
        mImageViewFilling.setVisibility(INVISIBLE);
        mImageViewFilling.setBackground(null);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.setMargins(0, 0, getResources().getDimensionPixelSize(R.dimen.margin_1_dp), 0);
        backGround.setLayoutParams(layoutParams);
        backGround.setVisibility(INVISIBLE);
        textView.setVisibility(INVISIBLE);
        textView.setVisibility(INVISIBLE);
        iconNoti.clearAnimation();
        iconOrder.clearAnimation();
        iconChat.clearAnimation();
        iconNoti.setVisibility(INVISIBLE);
        iconOrder.setVisibility(INVISIBLE);
        iconChat.setVisibility(INVISIBLE);

        searchAnim = ObjectAnimator.ofFloat(iconSearch, "x", size.x - iconSearch.getWidth(), bgSearchX);
        searchAnim.setDuration(400);
        searchAnim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                backGround.setVisibility(VISIBLE);
                backGround.setPivotX(0f);
                objectAnimator.start();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        animateIcons(iconChat, iconOrder, iconNoti);
                    }
                }, 500);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        searchAnim.start();

        final ValueAnimator valueAnimator = ValueAnimator.ofFloat(size.x - ((float) (textView.getWidth() * 1.1)), x);
        valueAnimator.setDuration(800);
        valueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                textView.setTranslationX((Float) animation.getAnimatedValue());
            }
        });
        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                animateTextSearch(0);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        objectAnimator = ObjectAnimator.ofFloat(backGround, "scaleX", 0.2f, 1.4f, 1f);
        objectAnimator.setDuration(1000);
        objectAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                search.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                isDoneAnimate = true;
                textView.setVisibility(View.VISIBLE);
                backGround.setVisibility(View.VISIBLE);
//                if (isPending()) {
//                    mImageViewFilling.setVisibility(VISIBLE);
//                    mImageViewFilling.setBackgroundResource(R.drawable.animation_star);
//                    ((AnimationDrawable) mImageViewFilling.getBackground()).start();
//                } else {
//                    mImageViewFilling.setVisibility(INVISIBLE);
//                    mImageViewFilling.setBackground(null);
//                }
                valueAnimator.start();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    public void startAnimationBeforeLogin() {
        iconSearch.setVisibility(VISIBLE);
        search.setVisibility(INVISIBLE);
//        mImageViewFilling.setVisibility(INVISIBLE);
//        mImageViewFilling.setBackground(null);
        backGround.clearAnimation();
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.setMargins(0, 0, getResources().getDimensionPixelSize(R.dimen.margin_1_dp), 0);
        backGround.setLayoutParams(layoutParams);
        backGround.setVisibility(INVISIBLE);
        textView.setVisibility(INVISIBLE);
        iconNoti.setVisibility(INVISIBLE);
        iconOrder.setVisibility(INVISIBLE);
        iconChat.setVisibility(INVISIBLE);

        searchAnim = ObjectAnimator.ofFloat(iconSearch, "x", size.x - iconSearch.getWidth(), bgSearchX);
        searchAnim.setDuration(400);
        searchAnim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                backGround.setVisibility(VISIBLE);
                search.setVisibility(VISIBLE);
                backGround.startAnimation(scaleAnimation);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        searchAnim.start();

        final ValueAnimator valueAnimator = ValueAnimator.ofFloat(size.x - 5, x);
        valueAnimator.setDuration(800);
        valueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                textView.setTranslationX((Float) animation.getAnimatedValue());
            }
        });
        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                animateTextSearch(0);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });


        scaleAnimation = new ScaleAnimation(0.1f, 1.0f, 1.0f, 1.0f, 0f, 1f);
        scaleAnimation.setDuration(1000);
        scaleAnimation.setInterpolator(new BounceInterpolator());
        scaleAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                textView.setVisibility(View.VISIBLE);
                backGround.setVisibility(View.VISIBLE);
                valueAnimator.start();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void animateTextSearch(int startDelay) {
        final ValueAnimator animator = ValueAnimator.ofFloat(1.0f, 1.08f, 0.95f, 1.05f, 1.0f, 1.02f, 1.0f);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                textView.setScaleX((Float) animation.getAnimatedValue());
                textView.setScaleY((Float) animation.getAnimatedValue());
            }
        });
        animator.setDuration(1500);
        animator.setStartDelay(startDelay);
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                animateTextSearch(3000);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animator.start();
    }

    private void animateIcons(final View... views) {
        for (int i = 0; i < views.length; i++) {
            Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.scale_icon_toolbar);
            animation.setStartOffset(100 * i);
            views[i].setVisibility(VISIBLE);
            views[i].startAnimation(animation);
        }
    }

    public void onMenuIconClick() {
//        UserProfileActivity.launch(getContext());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.material_menu_view:
                onMenuIconClick();
                break;
//            case R.id.background:
//                getContext().startActivity(new Intent(getContext(), SearchActivity.class));
//                break;
//            case R.id.layout_noti:
//                getContext().startActivity(new Intent(getContext(), NotificationActivity.class));
//                break;
//            case R.id.layout_order:
//                getContext().startActivity(new Intent(getContext(), CompletedOrderListActivity.class));
//                break;
//            case R.id.layout_chat:
//                getContext().startActivity(new Intent(getContext(), ChatV4Activity.class));
//                break;
        }
    }

    public MaterialMenuView getMenuView() {
        return (MaterialMenuView) findViewById(R.id.material_menu_view);
    }

    public void showOnBrandList(boolean isLogin) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        if (isLogin) {
            layoutParams.setMargins(0, 0, getResources().getDimensionPixelSize(R.dimen.margin_1_dp), 0);
            iconNoti.setVisibility(VISIBLE);
            iconOrder.setVisibility(VISIBLE);
            iconChat.setVisibility(VISIBLE);
        } else {
            layoutParams.setMargins(0, 0, getResources().getDimensionPixelSize(R.dimen.margin_1_dp), 0);
            iconNoti.setVisibility(INVISIBLE);
            iconOrder.setVisibility(INVISIBLE);
            iconChat.setVisibility(INVISIBLE);
        }
        backGround.setLayoutParams(layoutParams);
        iconSearch.setVisibility(VISIBLE);
        search.setVisibility(VISIBLE);
        backGround.setVisibility(VISIBLE);
        textView.setVisibility(VISIBLE);
    }

    public void showPendingAnimation(boolean pending) {
//        isPending = pending;
//        if (pending && isDoneAnimate) {
//            mImageViewFilling.setVisibility(View.VISIBLE);
//            mImageViewFilling.setBackgroundResource(R.drawable.animation_star);
//            ((AnimationDrawable) mImageViewFilling.getBackground()).start();
//        } else {
//            mImageViewFilling.setVisibility(View.INVISIBLE);
//            mImageViewFilling.setBackground(null);
//        }
    }

    /**
     * call this function for screen that no need param doneAnimate
     *
     * @param pending
     * @param doneAnimate
     */
    public void showPendingAnimation(boolean pending, boolean doneAnimate) {
        isDoneAnimate = doneAnimate;
        showPendingAnimation(pending);
    }

    public void showJumpAnimation(boolean jump) {
        isJump = jump;
        if (jump) {
            iconOrder.clearAnimation();
            ValueAnimator jumpAnimator = ValueAnimator.ofFloat(0, -30, 0, -20, 0, -10, 0, -5, 0);
            jumpAnimator.setDuration(2000);
            jumpAnimator.setRepeatCount(ValueAnimator.INFINITE);
            jumpAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    iconOrder.setTranslationY((Float) animation.getAnimatedValue());
                }
            });
            iconOrder.setTag(jumpAnimator);
            jumpAnimator.start();
        } else {
            try {
                iconOrder.setTranslationY(0);
                iconOrder.clearAnimation();
                ((ValueAnimator) iconOrder.getTag()).cancel();
            } catch (Exception e) {
                Log.w("", e.getMessage());
            }
        }
    }

    public void setBadge(int badgeCount, String type) {
        TextView badgeView = null;
        switch (type) {
            case "noti":
                badgeView = notiBadge;
                break;
            case "chat":
                badgeView = chatBadge;
                break;
            case "order":
                badgeView = orderBadge;
                break;
        }
        if (badgeView != null) {
            if (badgeCount > 0) {
                if (badgeCount < 100 && badgeCount > -1) {
                    badgeView.setText(String.format(Locale.ENGLISH, "%d", badgeCount));
                } else {
                    badgeView.setText("99+");
                }
                badgeView.setVisibility(View.VISIBLE);
            } else {
                badgeView.setVisibility(View.GONE);
            }
        }
    }

    public boolean isJump() {
        return isJump;
    }

    public boolean isPending() {
        return isPending;
    }
}
