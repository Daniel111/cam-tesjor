package kh.com.camtesjorapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.bumptech.glide.Glide;
import java.util.List;
import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.holder.CircleMemberViewHolder;
import kh.com.camtesjorapp.holder.MemberViewHolder;
import kh.com.camtesjorapp.model.TrackingModel;

public class MemberAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context mContext;
    private List<TrackingModel> mTrackingModels;
    private final int ADD_TYPE = -1;
    private int mLayoutType;
    
    public MemberAdapter(Context context,List<TrackingModel> trackingModels,int layoutType){
        this.mContext = context;
        this.mTrackingModels = trackingModels;
        this.mLayoutType = layoutType;
    }
    
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if(viewType == ADD_TYPE){
            View view = LayoutInflater.from(mContext).inflate(mLayoutType,parent,false);
            return new CircleMemberViewHolder(view);
        }
        View view = LayoutInflater.from(mContext).inflate(R.layout.member_item,parent,false);
        return new MemberViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof MemberViewHolder) {

            MemberViewHolder memberViewHolder = (MemberViewHolder) holder;
            Glide.with(mContext).load(mTrackingModels.get(position).getImage_url()).into(memberViewHolder.avatar);
            memberViewHolder.textViewName.setText(mTrackingModels.get(position).getFull_name());
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position == mTrackingModels.size()) ? ADD_TYPE : position;
    }

    @Override
    public int getItemCount() {
        return mTrackingModels.size() + 1;
    }
}
