package kh.com.camtesjorapp.mvp.signin.view;

import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import org.json.JSONObject;

public interface ISignInVIew {

    void onRetrieveDataSucceed(DataSnapshot snapshot);

    void onRetrieveDataError(DatabaseError databaseError);

    void onFacebookSignInSucceed(LoginResult loginResult);

    void onFacebookSignInFailed(FacebookException error);

    void onFacebookSignInCanceled();

    void onTokenSignInSucceed(Task<AuthResult> task);

    void onTokenSignInFailed(Task<AuthResult> task);

    void onEmailSignInSucceed(Task<AuthResult> task);

    void onEmailSignInFailed(Task<AuthResult> task);

    void onFbDataRespond(JSONObject jsonObject,LoginResult loginResult);

    void onFirebaseUserDataRespond(DataSnapshot dataSnapshot);
}
