package kh.com.camtesjorapp.controller;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import kh.com.camtesjorapp.interfaces.LocationUpdaterInterface;

public class LocationRequestController {

    private LocationRequest mLocationRequest;
    //    private long UPDATE_INTERVAL = 120 * 1000;  /* 2 mn */
    private long UPDATE_INTERVAL = 60 * 1000;  /* 60 secs */
    private long FASTEST_INTERVAL = 20 * 1000; /* 20 secs */
    private final int REQUEST_FINE_LOCATION = 123;
    private static final int TIME_DIFFERENCE_THRESHOLD = 60 * 1000;
    private LocationUpdaterInterface mLocationUpdaterInterface;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private Context mContext;
    private Activity mActivity;
    private LocationCallback locationListener;
    private Location mOldLocation;

    public LocationRequestController(Context mContext,int priority) {

        this.mContext = mContext;
        if (mContext instanceof Activity)
            this.mActivity = (Activity) mContext;
        startLocationUpdates(priority);
        getLastLocation();
    }

    @SuppressLint("RestrictedApi")
    private void startLocationUpdates(int priority) {

        // Create the location request to start receiving updates
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(priority);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setSmallestDisplacement(10);

        if (mActivity != null) {

            // Create LocationSettingsRequest object using location request
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
            builder.addLocationRequest(mLocationRequest);
            builder.setAlwaysShow(true);
            LocationSettingsRequest locationSettingsRequest = builder.build();

            Task<LocationSettingsResponse> result =
                    LocationServices.getSettingsClient(mContext).checkLocationSettings(builder.build());


            result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
                @Override
                public void onComplete(@NonNull Task<LocationSettingsResponse> task) {

                    try {
                        task.getResult(ApiException.class);
                    } catch (ApiException ex) {
                        switch (ex.getStatusCode()) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try {
                                    ResolvableApiException resolvableApiException = (ResolvableApiException) ex;
                                    resolvableApiException.startResolutionForResult(mActivity, 123);
                                } catch (IntentSender.SendIntentException e) {
                                    e.printStackTrace();
                                }
                                break;
                        }
                    }
                }
            });

            // Check whether location settings are satisfied
            // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
            SettingsClient settingsClient = LocationServices.getSettingsClient(mContext);
            settingsClient.checkLocationSettings(locationSettingsRequest);
        }

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(mContext);

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)

        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        locationListener = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (isBetterLocation(mOldLocation, locationResult.getLastLocation())) {
                    // If location is better, do some user preview.
                    onLocationChanged(locationResult.getLastLocation());
                }

                mOldLocation = locationResult.getLastLocation();

            }
        };
        fusedLocationProviderClient.requestLocationUpdates(mLocationRequest, locationListener, Looper.myLooper());
    }

    public void onLocationChanged(Location location) {

        if (mLocationUpdaterInterface != null && location != null) {
            mLocationUpdaterInterface.onLocationCallBack("location", location);
        }
    }

    public void removeLocationUpdate() {

        try {
            if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) !=
                            PackageManager.PERMISSION_GRANTED) {
                return;
            }

            fusedLocationProviderClient.removeLocationUpdates(locationListener);
            mLocationUpdaterInterface = null;

        } catch (Exception ex) {
            Log.d("On Remove", "fail to remove location listener, ignore", ex);
        }

    }

    public void checkPermissions() {
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED)
            requestPermissions();
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(mActivity,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_FINE_LOCATION);
    }

    public void checkPermissions(Fragment fragment) {
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(fragment);
        }
    }

    private void requestPermissions(Fragment fragment) {
        fragment.requestPermissions(
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_FINE_LOCATION);
    }

    public void getLastLocation() {

        // Get last known recent location using new Google Play Services SDK (v11+)
        if (ActivityCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            return;

        fusedLocationProviderClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // GPS location can be null if GPS is switched off
                        if (location != null) {
//                            onLocationChanged(location);
                            if (mLocationUpdaterInterface != null)
                                mLocationUpdaterInterface.onLocationCallBack("LastLocation", location);

                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("MapDemoActivity", "Error trying to get last GPS location");
                        e.printStackTrace();
                    }
                });
    }

    public void onPermissionResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                && requestCode == REQUEST_FINE_LOCATION) {
            startLocationUpdates(LocationRequest.PRIORITY_HIGH_ACCURACY);
        }
    }

    public void setLocationUpdateListener(LocationUpdaterInterface updateLocationListener) {
        mLocationUpdaterInterface = updateLocationListener;
    }

    private boolean isBetterLocation(Location oldLocation, Location newLocation) {
        // If there is no old location, of course the new location is better.
        if (oldLocation == null) {
            return true;
        }

        // Check if new location is newer in time.
        boolean isNewer = newLocation.getTime() > oldLocation.getTime();

        // Check if new location more accurate. Accuracy is radius in meters, so less is better.
        boolean isMoreAccurate = newLocation.getAccuracy() < oldLocation.getAccuracy();
        if (isMoreAccurate && isNewer) {
            // More accurate and newer is always better.
            return true;
        } else if (isMoreAccurate && !isNewer) {
            // More accurate but not newer can lead to bad fix because of user movement.
            // Let us set a threshold for the maximum tolerance of time difference.
            long timeDifference = newLocation.getTime() - oldLocation.getTime();

            // If time difference is not greater then allowed threshold we accept it.
            if (timeDifference > -TIME_DIFFERENCE_THRESHOLD) {
                return true;
            }
        }

        return false;
    }
}
