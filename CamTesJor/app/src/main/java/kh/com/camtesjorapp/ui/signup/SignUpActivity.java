package kh.com.camtesjorapp.ui.signup;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.onesignal.OneSignal;

import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.base.BaseActivity;
import kh.com.camtesjorapp.model.UserModel;
import kh.com.camtesjorapp.mvp.signup.presenter.SignUpPresenter;
import kh.com.camtesjorapp.mvp.signup.view.ISignUpView;
import kh.com.camtesjorapp.sharepref.UserSharePref;
import kh.com.camtesjorapp.utils.Utils;

import static kh.com.camtesjorapp.utils.Utils.setError;

public class SignUpActivity extends BaseActivity implements ISignUpView {

    private EditText firstName;
    private EditText lastName;
    private EditText email;
    private EditText password;
    private EditText confirmPass;
    private ProgressDialog progressDialog;
    public static final int REGISTER_REQUEST_CODE = 456;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        init();
    }

    @Override
    public boolean isDisplaySearchBar() {
        return false;
    }

    @Override
    protected String getToolbarTitle() {
        return "Sign Up";
    }

    @Override
    public void onCreateAccountSucceed(Task<AuthResult> task) {
        progressDialog.dismiss();
        saveUserInfo(task);
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onCreateAccountFail() {
        progressDialog.dismiss();
    }

    private void init() {

        firstName = findViewById(R.id.first_name);
        lastName = findViewById(R.id.last_name);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        findViewById(R.id.btn_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });
        progressDialog = new ProgressDialog(this);
        confirmPass = findViewById(R.id.confirm_password);
        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!Utils.isEmailValid(s.toString()) && !s.toString().isEmpty()) {
                    email.setError("Email is invalid.");
                }
            }
        });
        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() < 6)
                    setError(password, "Your password must have at least 6 characters.");

            }
        });
    }

    private void register() {
        if (validate()) {
            if (!((CheckBox) findViewById(R.id.checkbox_term)).isChecked())
                Toast.makeText(SignUpActivity.this, "Please check to agree Term of Service.", Toast.LENGTH_SHORT).show();
            else
                submit();
        }
    }

    private void submit() {
        progressDialog.setMessage("Signing up...");
        progressDialog.show();
        new SignUpPresenter(this).onCreateAccount(Utils.getText(email),Utils.getText(password));
    }

    private void saveUserInfo(Task<AuthResult> task) {

        DatabaseReference currentDatabase = FirebaseDatabase.
                getInstance().getReference().child("users").
                child(task.getResult().getUser().getUid());

        UserModel userModel = new UserModel();
        userModel.setEmail(task.getResult().getUser().getEmail());
        userModel.setUid(task.getResult().getUser().getUid());
        userModel.setFirst_name(Utils.getText(firstName));
        userModel.setLast_name(Utils.getText(lastName));
        userModel.setImage_url("https://gmatclub.com/forum/images/avatars/upload/avatar_719865.jpg");
        userModel.setFull_name(Utils.getText(firstName) + " " + Utils.getText(lastName));

        UserSharePref.getInstance(this).setUserInfo(userModel);
        currentDatabase.setValue(userModel);
        OneSignal.sendTag("User_ID",task.getResult().getUser().getUid());
    }

    private boolean validate() {

        boolean validate = true;

        if (!Utils.isEmailValid(Utils.getText(email))) {
            setError(email, "Email is invalid.");
            validate = false;
        }

        if (TextUtils.isEmpty(Utils.getText(firstName))) {
            setError(firstName, "First name is required.");
            validate = false;
        }
        if (TextUtils.isEmpty(Utils.getText(lastName))) {
            setError(lastName, "Last name is required.");
            validate = false;
        }
        if (TextUtils.isEmpty(Utils.getText(email))) {
            setError(email, "Email name is required.");
            validate = false;
        }

        if (TextUtils.isEmpty(Utils.getText(confirmPass))) {
            setError(confirmPass, "Confirm password is required.");
            validate = false;
        }

        if (!Utils.getText(password).equals(Utils.getText(confirmPass))) {
            setError(confirmPass, "Password dose not match.");
            validate = false;
        }

        if (TextUtils.isEmpty(Utils.getText(password))) {
            setError(password, "Password is required.");
            validate = false;
        } else if (Utils.getText(password).length() < 6) {
            setError(password, "Your password must have at least 6 characters.");
            validate = false;
        }

        return validate;
    }

    public static void lunch(Context context) {
        Intent intent = new Intent(context, SignUpActivity.class);
        ((Activity)context).startActivityForResult(intent,REGISTER_REQUEST_CODE);
    }
}
