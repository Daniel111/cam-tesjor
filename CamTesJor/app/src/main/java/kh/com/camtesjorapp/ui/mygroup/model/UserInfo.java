package kh.com.camtesjorapp.ui.mygroup.model;

/**
 * Created by mac on 3/15/2018 AD.
 */

public class UserInfo {

    public String full_name;
    public String group_admin;
    public String image_url;
    public String last_name;
    public String first_name;


    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public UserInfo(){}

    public UserInfo(String full_name, String group_admin) {
        this.full_name = full_name;
        this.group_admin = group_admin;
    }

    public String getGroup_admin() {
        return group_admin;
    }

    public void setGroup_admin(String group_admin) {
        this.group_admin = group_admin;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }
}
