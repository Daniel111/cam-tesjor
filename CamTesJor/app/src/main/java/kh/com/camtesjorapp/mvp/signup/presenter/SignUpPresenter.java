package kh.com.camtesjorapp.mvp.signup.presenter;

import android.support.annotation.NonNull;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import kh.com.camtesjorapp.mvp.signup.view.ISignUpView;

public class SignUpPresenter implements ISignUpPresenter {

    private ISignUpView mISignUpView;

    public SignUpPresenter(ISignUpView iSignUpView){
        mISignUpView = iSignUpView;
    }

    @Override
    public void onCreateAccount(String email, String pass) {

        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuth.createUserWithEmailAndPassword(email,pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()) mISignUpView.onCreateAccountSucceed(task);
                else mISignUpView.onCreateAccountFail();
            }
        });

    }
}
