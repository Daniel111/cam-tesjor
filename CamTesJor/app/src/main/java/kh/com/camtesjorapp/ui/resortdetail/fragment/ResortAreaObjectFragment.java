package kh.com.camtesjorapp.ui.resortdetail.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

import kh.com.camtesjorapp.R;
import kh.com.camtesjorapp.constant.Constants;
import kh.com.camtesjorapp.ui.resortdetail.presenter.IResortDetailPresenter;
import kh.com.camtesjorapp.ui.resortdetail.presenter.ResortDetailPresenter;
import kh.com.camtesjorapp.ui.resortdetail.view.IResortDetailView;

public class ResortAreaObjectFragment extends Fragment implements IResortDetailView, OnMapReadyCallback {

    private View mRootView ;
    private GoogleMap mGoogleMap;
    private MapView mMapView;
    private double latitude;
    private double longitude;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

         return inflater.inflate(R.layout.fragment_resort_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.mRootView = view;

        mMapView = view.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);

        IResortDetailPresenter mIResortDetailPresenter = new
                ResortDetailPresenter(this,
                getActivity().getIntent().getStringExtra(Constants.RESORT_DETAIL_KEY));

        mIResortDetailPresenter.retrieveData();
    }

    @Override
    public void onRetrieveDataSucceed(DataSnapshot snapshot) {

        TextView textView = (TextView) mRootView.findViewById(R.id.tv_resort_detail);
        textView.setText(snapshot.child("description").getValue().toString());
    }

    @Override
    public void onRetrieveDataError(DatabaseError databaseError) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.getUiSettings().setScrollGesturesEnabled(false);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        mGoogleMap.getUiSettings().setZoomGesturesEnabled(false);
        displayMapMarker();
    }

    private void displayMapMarker() {

//        13.364047, 103.860313.

        latitude = 13.364047;
        longitude = 103.860313;

//        if (latitude > 0 && longitude > 0) {
            final LatLng mPosition = new LatLng(latitude, longitude);
            CameraPosition mCameraPosition = CameraPosition.builder().target(mPosition).zoom(14f).bearing(0.0f).tilt(0.0f).build();
            mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(mCameraPosition));
            mGoogleMap.addMarker(new MarkerOptions().position(mPosition));
//        }
    }

    @Override
    public void onResume() {
        if (null != mMapView) {
            mMapView.onResume();
        }
        super.onResume();
    }

    @Override
    public void onDestroy() {
        if (null != mMapView) {
            mMapView.onDestroy();
        }
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        if (null != mMapView) {
            mMapView.onLowMemory();
        }
        super.onLowMemory();
    }

    @Override
    public void onPause() {
        if (null != mMapView) {
            mMapView.onPause();
        }
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (null != mMapView) {
            mMapView.onSaveInstanceState(outState);
        }
    }
}
