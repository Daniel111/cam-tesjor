package kh.com.camtesjorapp.mvp.member.presenter;

public interface IMemberPresenter {

    void onGetDataFromGroupAdmin(String groupId);
    void onGetDataFromGroupJoining(String groupId);

}
