package kh.com.camtesjorapp.model;

/**
 * Created by mac on 5/4/2018 AD.
 */

public class BatteryModel {

    private boolean charging;
    private float level;
    private float scale;
    private long time_stamp;

    public BatteryModel() {
    }

    public boolean isCharging() {
        return charging;
    }

    public void setCharging(boolean charging) {
        this.charging = charging;
    }

    public float getLevel() {
        return level;
    }

    public void setLevel(float level) {
        this.level = level;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    public long getTime_stamp() {
        return time_stamp;
    }

    public void setTime_stamp(long time_stamp) {
        this.time_stamp = time_stamp;
    }
}
