package kh.com.camtesjorapp.ui.resortdetail.fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kh.com.camtesjorapp.R;

/**
 * Created by mac on 5/29/2017 AD.
 */

public class HotelObjectFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_hotel, container, false);
        return rootView;
    }
}
